<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MessengerType;
use BmPlatform\Abstraction\Enums\VariableCategory;
use BmPlatform\ApiModule\ApiClient;
use BmPlatform\ApiModule\ApiIntegrationCommands;
use BmPlatform\ApiModule\Entities\Integration;
use BmPlatform\ApiModule\Entities\Schema;
use BmPlatform\ApiModule\Entities\Variables\Variable;
use Illuminate\Support\Facades\App;
use Mockery;

class ApiIntegrationCommandsTest extends TestCase
{
    private ApiClient|Mockery\MockInterface $api_client;
    private ApiIntegrationCommands $api_commands;

    public function setUp(): void
    {
        parent::setUp();

        $this->api_client = Mockery::mock(ApiClient::class);

        App::expects('makeWith')->with(ApiClient::class, [
            'base_url'   => 'https://example.com',
            'secret_key' => 'something_secret'
        ])->once()->andReturn($this->api_client);

        $this->api_commands = new ApiIntegrationCommands(
            new Integration(
                code: 'api',
                url: 'https://example.com',
                key: 'something_secret'
            ),
        );
    }

    public function tearDown(): void
    {
        parent::tearDown();

        unset($this->api_commands, $this->api_client);
    }

    public function testSchema(): void
    {
        $this->mockTranslatorIntoDI();

        $this->api_client->expects('get')
            ->with('/schema')
            ->once()
            ->andReturn($this->getSchemaPartnerResponseData());

        $schema = $this->api_commands->getSchema();

        $this->assertInstanceOf(Schema::class, $schema);
        $this->assertEquals($this->getSchemaPartnerResponseData(), $schema->getSchema());
        $this->assertEquals([], $schema->getVariables());
    }

    public function testSchemaWithVariables(): void
    {
        $this->mockTranslatorIntoDI();

        $schema_response_data = $this->getSchemaPartnerResponseData();
        $schema_response_data['variables'] = [
            [
                'key'                => 'dealId',
                'type'               => 'int',
                'resolver'           => 'chat.extraData.dealId',
                'name'               => 'Deal ID',
                'category'           => 'shop',
                'hideFromConditions' => true
            ]
        ];

        $this->api_client->expects('get')
            ->with('/schema')
            ->once()
            ->andReturn($schema_response_data);

        $schema = $this->api_commands->getSchema();

        $this->assertInstanceOf(Schema::class, $schema);
        $this->assertEquals($this->getSchemaPartnerResponseData(), $schema->getSchema());
        $this->assertEquals([
            tap(new Variable('dealId', 'int', 'Deal ID'), function (Variable $variable) {
                $variable->resolver = 'chat.extraData.dealId';
                $variable->category = VariableCategory::shop;
                $variable->hide_from_conditions = true;
            })
        ], $schema->getVariables());
    }

    public function testSchemaOnIncorrectSchema(): void
    {
        $schema = [
            'events' => [
                'inboxFlags' => [
                    'newChatCreated'      => false,
                    'newTicketOpened'     => false,
                    'externalPostComment' => true,
                ],
            ],

            'messenger_types' => [
                'default' => [
                    'text' => [
                        'max_length' => 4096,
                    ],
                ],
            ]
        ];

        $this->api_client->expects('get')
            ->with('/schema')
            ->once()
            ->andReturn($schema);

        $this->expectErrorExceptionWithCode(
            ErrorCode::InvalidResponseData,
            fn () => $this->api_commands->getSchema()
        );
    }

    public function testSchemaOnIncorrectVariable(): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Invalid!');

        $schema_response_data = $this->getSchemaPartnerResponseData();
        $schema_response_data['variables'] = [
            [
                'key'                => 'dealId',
                'type'               => 'integer',
                'resolver'           => 'chat.extraData.dealId',
                'name'               => 'Deal ID',
                'category'           => 'shop',
                'hideFromConditions' => true
            ]
        ];

        $this->api_client->expects('get')
            ->with('/schema')
            ->once()
            ->andReturn($schema_response_data);

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => $this->api_commands->getSchema()
        );
    }

    private function getSchemaPartnerResponseData(): array
    {
        return [
            'events' => [
                'inboxFlags' => [
                    'newChatCreated'      => false,
                    'newTicketOpened'     => false,
                    'externalPostComment' => true,
                ],
                'outboxSent'            => true,
                'newContactChatAdded'   => true,
                'chatTicketClosed'      => false,
                'chatTransferred'       => true,
                'chatTagAttached'       => false,
                'chatTagDetached'       => false,
                'chatTicketTagAttached' => false,
                'chatTicketTagDetached' => false,
                'messageStatusChanged'  => false,
                'contactDataUpdated'    => true,
            ],

            'messenger_types' => [
                'default' => [
                    'name' => 'Messenger',

                    'inline_buttons' => [
                        'text'               => false,
                        'text_max_length'    => 1,
                        'url'                => false,
                        'payload'            => false,
                        'payload_max_length' => 1,
                        'colors'             => false,
                        'max_count'          => 0,
                    ],

                    'quick_replies' => [
                        'text'            => false,
                        'text_max_length' => 1,
                        'phone'           => false,
                        'geolocation'     => false,
                        'colors'          => false,
                        'max_count'       => 0,
                    ],

                    'text' => [
                        'max_length' => 4096,
                    ],
                ],

                'whatsapp' => [
                    'extends'       => 'default',
                    'name'          => 'WhatsApp by Twilio',
                    'internal_type' => MessengerType::Whatsapp->value
                ],

                'one_more_chines_messenger' => [
                    'extends' => 'default',
                    'name'    => 'One more Chines Messenger',
                ],
            ]
        ];
    }
}
