<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Http;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Exceptions\ErrorException;
use BmPlatform\ApiModule\Http\IntegrationController;
use BmPlatform\ApiModule\Tests\TestCase;
use BmPlatform\ApiModule\UseCases\CreateUser;
use BmPlatform\ApiModule\UseCases\DeactivateUser;
use BmPlatform\ApiModule\UseCases\ProcessEvent;
use Illuminate\Container\Container;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Mockery;

class IntegrationControllerTest extends TestCase
{
    public function testCreateUser(): void
    {
        $use_case = Mockery::mock(CreateUser::class);
        $use_case->expects('execute')
            ->with('api1', ['externalId' => '10'])
            ->once()
            ->andReturn('https://example.com/?access_token=abc');

        $this->expectsResponseJson(['webUrl' => 'https://example.com/?access_token=abc'], 200);

        $controller = new IntegrationController();
        $response = $controller->createUser(
            'api1',
            new Request(['externalId' => '10']),
            $use_case
        );

        $this->assertEquals('{"webUrl":"https:\/\/example.com\/?access_token=abc"}', $response->getContent());
    }

    public function testCreateUserOnException(): void
    {
        $use_case = Mockery::mock(CreateUser::class);
        $use_case->expects('execute')
            ->with('api1', ['externalId' => '10'])
            ->once()
            ->andThrow(new ErrorException(ErrorCode::DataMissing, 'data missing'));

        $this->expectsResponseJson([
            'message' => 'Api Module data missing',
            'code'    => 'DataMissing'
        ], 400);

        $controller = new IntegrationController();
        $response = $controller->createUser(
            'api1',
            new Request(['externalId' => '10']),
            $use_case
        );

        $this->assertEquals('{"message":"Api Module data missing","code":"DataMissing"}', $response->getContent());
    }

    public function testDeactivateUser(): void
    {
        $use_case = Mockery::mock(DeactivateUser::class);
        $use_case->expects('execute')
            ->with('api1', '10')
            ->once();

        $this->expectsResponseJson(null, 202);

        $controller = new IntegrationController();
        $response = $controller->deactivateUser(
            'api1',
            '10',
            $use_case
        );

        $this->assertEquals('{}', $response->getContent());
    }

    public function testDeactivateUserOnException(): void
    {
        $use_case = Mockery::mock(DeactivateUser::class);
        $use_case->expects('execute')
            ->with('api1', '10')
            ->once()
            ->andThrow(new ErrorException(ErrorCode::DataMissing, 'data missing'));

        $this->expectsResponseJson([
            'message' => 'Api Module data missing',
            'code'    => 'DataMissing'
        ], 400);

        $controller = new IntegrationController();
        $response = $controller->deactivateUser(
            'api1',
            '10',
            $use_case
        );

        $this->assertEquals('{"message":"Api Module data missing","code":"DataMissing"}', $response->getContent());
    }

    public function testProcessEvent(): void
    {
        $use_case = Mockery::mock(ProcessEvent::class);
        $use_case->expects('execute')
            ->with('api1', '10', ['eventType' => 'do-it!'])
            ->once();

        $this->expectsResponseJson(null, 202);

        $controller = new IntegrationController();
        $response = $controller->processEvent(
            'api1',
            '10',
            new Request(['eventType' => 'do-it!']),
            $use_case
        );

        $this->assertEquals('{}', $response->getContent());
    }

    public function testProcessEventOnException(): void
    {
        $use_case = Mockery::mock(ProcessEvent::class);
        $use_case->expects('execute')
            ->with('api1', '10', ['eventType' => 'do-it!'])
            ->once()
            ->andThrow(new ErrorException(ErrorCode::DataMissing, 'data missing'));

        $this->expectsResponseJson([
            'message' => 'Api Module data missing',
            'code'    => 'DataMissing'
        ], 400);

        $controller = new IntegrationController();
        $response = $controller->processEvent(
            'api1',
            '10',
            new Request(['eventType' => 'do-it!']),
            $use_case
        );

        $this->assertEquals('{"message":"Api Module data missing","code":"DataMissing"}', $response->getContent());
    }

    private function expectsResponseJson(?array $data, int $code): void
    {
        $response_factory = Mockery::mock(ResponseFactory::class);
        $response_factory->expects('json')
            ->with($data, $code)
            ->andReturn(new JsonResponse($data));

        $container = Mockery::mock(Container::class);
        $container->expects('make')
            ->with(ResponseFactory::class, [])
            ->once()
            ->andReturn($response_factory);

        Container::setInstance($container);
    }
}
