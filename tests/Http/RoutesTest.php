<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Http;

use BmPlatform\ApiModule\Http\BearerTokenMiddleware;
use BmPlatform\ApiModule\Tests\TestCase;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Routing\RouteCollectionInterface;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\App;
use Mockery;

class RoutesTest extends TestCase
{
    public function testRoutesRegister()
    {
        $router = new Router(
            Mockery::mock(Dispatcher::class),
            Mockery::mock(Container::class)
        );

        App::expects('make')
            ->with('router')
            ->once()
            ->andReturn($router);

        require __DIR__ . '/../../src/Http/routes.php';

        $routes = $router->getRoutes();
        $routes->refreshNameLookups();

        $this->assertRoute($routes, 'api-module.users.create', 'POST', 'integrations/api/{code}');
        $this->assertRoute($routes, 'api-module.users.destroy', 'DELETE', 'integrations/api/{code}/{id}');
        $this->assertRoute($routes, 'api-module.events.store', 'POST', 'integrations/api/{code}/{id}/event');
    }

    private function assertRoute(RouteCollectionInterface $routes, string $name, string $method, string $uri): void
    {
        $route = $routes->getByName($name);

        $this->assertNotNull($route);
        $this->assertEquals([$method], $route->methods());
        $this->assertEquals($uri, $route->uri());
        $this->assertEquals([BearerTokenMiddleware::class], $route->middleware());
    }
}
