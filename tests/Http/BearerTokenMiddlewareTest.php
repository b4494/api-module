<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Http;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Entities\Integration;
use BmPlatform\ApiModule\Http\BearerTokenMiddleware;
use BmPlatform\ApiModule\IntegrationRepository;
use BmPlatform\ApiModule\Tests\TestCase;
use Illuminate\Http\Request;
use Mockery;

class BearerTokenMiddlewareTest extends TestCase
{
    private BearerTokenMiddleware $middleware;

    public function setUp(): void
    {
        parent::setUp();

        $integration_repository = Mockery::mock(IntegrationRepository::class);
        $integration_repository->expects('getByCodeOrFail')->andReturn(new Integration(
            code: 'api1',
            url: 'https://example.com',
            key: 'secret'
        ));

        $this->middleware = new BearerTokenMiddleware($integration_repository);
    }

    public function testHandle(): void
    {
        $next_handle_called = false;

        $next_handler = function () use (&$next_handle_called) {
            $next_handle_called = true;
        };

        $this->middleware->handle(
            new Request(server: ['HTTP_AUTHORIZATION' => 'Bearer secret']),
            $next_handler
        );

        $this->assertTrue($next_handle_called);
    }

    public function testHandleOnFailure(): void
    {
        $this->expectErrorExceptionWithCode(
            ErrorCode::AuthenticationFailed,
            fn () => $this->middleware->handle(
                new Request(server: ['HTTP_AUTHORIZATION' => 'Bearer secret!']),
                fn () => false
            )
        );
    }
}
