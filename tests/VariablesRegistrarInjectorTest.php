<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests;

use BmPlatform\Abstraction\Enums\VariableCategory;
use BmPlatform\Abstraction\Interfaces\Chat as ChatInterface;
use BmPlatform\Abstraction\Interfaces\RuntimeContext;
use BmPlatform\Abstraction\Interfaces\Variable as VariableInterface;
use BmPlatform\Abstraction\Interfaces\VariableRegistrar;
use BmPlatform\ApiModule\Entities\Variables\ComplexVariable;
use BmPlatform\ApiModule\Entities\Variables\EnumVariable;
use BmPlatform\ApiModule\Entities\Variables\EnumVariableOption;
use BmPlatform\ApiModule\Entities\Variables\Variable;
use BmPlatform\ApiModule\Entities\Variables\VariableResolver;
use BmPlatform\ApiModule\VariablesRegistrarInjector;
use Closure;
use Mockery;
use Mockery\MockInterface;

class VariablesRegistrarInjectorTest extends TestCase
{
    private VariablesRegistrarInjector $variables_registrar_injector;
    private VariableRegistrar|MockInterface $registrar;

    public function setUp(): void
    {
        parent::setUp();

        $this->registrar = Mockery::mock(VariableRegistrar::class);

        $this->variables_registrar_injector = new VariablesRegistrarInjector($this->registrar);
    }

    public function tearDown(): void
    {
        parent::tearDown();

        unset($this->registrar, $this->variables_registrar_injector);
    }

    public function testInjectOnBaseVariable(): void
    {
        $variable = new Variable('dealId', 'int', 'Deal ID');
        $variable->resolver = 'chat.extraData.dealId';
        $variable->category = VariableCategory::shop;
        $variable->array    = false;
        $variable->hide_from_conditions = false;

        $app_variable = Mockery::mock(VariableInterface::class);
        $app_variable->expects('name')->with('Deal ID')->once()->andReturnSelf();
        $app_variable->expects('array')->with(false)->once()->andReturnSelf();
        $app_variable->expects('category')->with(VariableCategory::shop)->once()->andReturnSelf();
        $app_variable->expects('hideFromConditions')->with(false)->once()->andReturnSelf();

        $this->registrar->expects('int')->with('dealId', Mockery::type(Closure::class))->once()->andReturn($app_variable);

        $this->variables_registrar_injector->inject($variable);
    }

    public function testInjectOnEnumVariable(): void
    {
        $variable = new EnumVariable('enumDeal', 'string', [
            new EnumVariableOption('Simple Deal', 'deal_simple'),
            new EnumVariableOption('Complex Deal', 'deal_complex'),
        ]);

        $variable->resolver = null;
        $variable->category = 'internal_kitchen';
        $variable->hide_from_conditions = true;

        $app_variable = Mockery::mock(VariableInterface::class);
        $app_variable->expects('name')->with('string')->once()->andReturnSelf();
        $app_variable->expects('category')->with('internal_kitchen')->once()->andReturnSelf();
        $app_variable->expects('hideFromConditions')->with(true)->once()->andReturnSelf();

        $this->registrar->expects('enum')->with('enumDeal', [
            [
                'label' => 'Simple Deal',
                'value' => 'deal_simple'
            ],
            [
                'label' => 'Complex Deal',
                'value' => 'deal_complex'
            ]
        ], null)->once()->andReturn($app_variable);

        $this->variables_registrar_injector->inject($variable);
    }

    public function testInjectOnComplexVariable(): void
    {
        $property_1_level_3 = new Variable('amount', 'float', 'Deal Amount');
        $property_1_level_3->resolver = 'chat.extraData.deal_amount';

        $property_2_level_3 = new EnumVariable('currency', 'Deal Amount Currency', [
            new EnumVariableOption('USD', 'usd'),
            new EnumVariableOption('EUR', 'eur'),
        ]);
        $property_2_level_3->resolver = 'chat.extraData.deal_currency';

        $property_1_level_2 = new Variable('id', 'pk', 'Deal ID');
        $property_1_level_2->resolver = 'chat.extraData.dealId';
        $property_1_level_2->category = VariableCategory::shop;

        $property_2_level_2 = new Variable('name', 'text', 'Deal Name');
        $property_2_level_2->resolver = 'chat.extraData.deal_name';

        $property_3_level_2 = new ComplexVariable('total', 'Deal Total', [$property_1_level_3, $property_2_level_3]);

        $variable = new ComplexVariable('deal', 'Deal', [$property_1_level_2, $property_2_level_2, $property_3_level_2]);

        $app_variable = Mockery::mock(VariableInterface::class);
        $app_variable->expects('name')->with('Deal')->once()->andReturnSelf();

        $this->registrar->expects('complex')->withArgs(function (string $key, Closure $closure) {
            $this->assertEquals('deal', $key);

            $id_app_variable = Mockery::mock(VariableInterface::class);
            $id_app_variable->expects('name')->with('Deal ID')->once()->andReturnSelf();
            $id_app_variable->expects('category')->with(VariableCategory::shop)->once()->andReturnSelf();

            $name_app_variable = Mockery::mock(VariableInterface::class);
            $name_app_variable->expects('name')->with('Deal Name')->once()->andReturnSelf();

            $total_app_variable = Mockery::mock(VariableInterface::class);
            $total_app_variable->expects('name')->with('Deal Total')->once()->andReturnSelf();

            $sub_registrar = Mockery::mock(VariableRegistrar::class);
            $sub_registrar->expects('pk')->with('id', Mockery::type(Closure::class))->once()->andReturn($id_app_variable);
            $sub_registrar->expects('text')->with('name', Mockery::type(Closure::class))->once()->andReturn($name_app_variable);

            $sub_registrar->expects('complex')->withArgs(function (string $key, Closure $closure) {
                $this->assertEquals('total', $key);

                $total_amount_app_variable = Mockery::mock(VariableInterface::class);
                $total_amount_app_variable->expects('name')->with('Deal Amount')->once()->andReturnSelf();

                $total_currency_app_variable = Mockery::mock(VariableInterface::class);
                $total_currency_app_variable->expects('name')->with('Deal Amount Currency')->once()->andReturnSelf();

                $sub_registrar = Mockery::mock(VariableRegistrar::class);
                $sub_registrar->expects('float')->with('amount', Mockery::type(Closure::class))->once()->andReturn($total_amount_app_variable);
                $sub_registrar->expects('enum')->with('currency', [
                    ['label' => 'USD', 'value' => 'usd'],
                    ['label' => 'EUR', 'value' => 'eur']
                ], Mockery::type(Closure::class))->once()->andReturn($total_currency_app_variable);

                $closure($sub_registrar);

                return true;
            })->once()->andReturn($total_app_variable);

            $closure($sub_registrar);

            return true;
        })->once()->andReturn($app_variable);

        $this->variables_registrar_injector->inject($variable);
    }

    public function testInjectOnBaseVariableOnIncorrectType(): void
    {
        $variable = new Variable('dealId', 'biginteger', 'Deal ID');

        $this->registrar->shouldNotHaveBeenCalled();

        $this->variables_registrar_injector->inject($variable);
    }

    public function testInjectOnBaseVariableExtractValueResolverChatExtraData(): void
    {
        $closure_finished = false;

        $variable = new Variable('dealId', 'int', 'Deal ID');
        $variable->resolver = 'chat.extraData.dealId';

        $app_variable = Mockery::mock(VariableInterface::class);
        $app_variable->expects('name')->with('Deal ID')->once()->andReturnSelf();

        $this->registrar->expects('int')->withArgs(function (string $key, Closure $closure) use (&$closure_finished) {
            $this->assertEquals('dealId', $key);

            $context = Mockery::mock(RuntimeContext::class);
            $context->expects('chat->getExtraData')->once()->andReturn([
                'dealId' => '10'
            ]);

            $this->assertEquals('10', $closure($context));

            $closure_finished = true;

            return true;
        })->once()->andReturn($app_variable);

        $this->variables_registrar_injector->inject($variable);

        $this->assertTrue($closure_finished);
    }

    public function testInjectOnBaseVariableExtractValueResolverChatContactExtraFields(): void
    {
        $closure_finished = false;

        $variable = new Variable('dealId', 'int', 'Deal ID');
        $variable->resolver = 'chat.contact.extraFields.dealId';

        $app_variable = Mockery::mock(VariableInterface::class);
        $app_variable->expects('name')->with('Deal ID')->once()->andReturnSelf();

        $this->registrar->expects('int')->withArgs(function (string $key, Closure $closure) use (&$closure_finished) {
            $this->assertEquals('dealId', $key);

            $context = Mockery::mock(RuntimeContext::class);
            $context->expects('chat->getContact->getExtraFields')->once()->andReturn([
                'dealId' => '10'
            ]);

            $this->assertEquals('10', $closure($context));

            $closure_finished = true;

            return true;
        })->once()->andReturn($app_variable);

        $this->variables_registrar_injector->inject($variable);

        $this->assertTrue($closure_finished);
    }

    public function testInjectOnBaseVariableExtractValueResolverIncorrectPath(): void
    {
        $closure_finished = false;

        $variable = new Variable('dealId', 'int', 'Deal ID');
        $variable->resolver = 'chat.dialId';

        $app_variable = Mockery::mock(VariableInterface::class);
        $app_variable->expects('name')->with('Deal ID')->once()->andReturnSelf();

        $this->registrar->expects('int')->withArgs(function (string $key, Closure $closure) use (&$closure_finished) {
            $this->assertEquals('dealId', $key);

            $context = Mockery::mock(RuntimeContext::class);
            $context->expects('chat')->once()->andReturn(Mockery::mock(ChatInterface::class));

            $this->assertNull($closure($context));

            $closure_finished = true;

            return true;
        })->once()->andReturn($app_variable);

        $this->variables_registrar_injector->inject($variable);

        $this->assertTrue($closure_finished);
    }

    public function testInjectOnBaseVariableExtractValueResolverTryingToGetItemFromPrimitive(): void
    {
        $closure_finished = false;

        $variable = new Variable('dealId', 'int', 'Deal ID');
        $variable->resolver = 'chat.contact.extraFields.deal.id';

        $app_variable = Mockery::mock(VariableInterface::class);
        $app_variable->expects('name')->with('Deal ID')->once()->andReturnSelf();

        $this->registrar->expects('int')->withArgs(function (string $key, Closure $closure) use (&$closure_finished) {
            $this->assertEquals('dealId', $key);

            $context = Mockery::mock(RuntimeContext::class);
            $context->expects('chat->getContact->getExtraFields')->once()->andReturn([
                'dealId' => '10'
            ]);

            $this->assertNull($closure($context));

            $closure_finished = true;

            return true;
        })->once()->andReturn($app_variable);

        $this->variables_registrar_injector->inject($variable);

        $this->assertTrue($closure_finished);
    }

    public function testInjectOnBaseVariableExtractValueResolverCacheValue(): void
    {
        $closure_finished = false;

        $variable = new Variable('dealId', 'int', 'Deal ID');
        $variable->resolver = 'cacheValue.dialId';

        $app_variable = Mockery::mock(VariableInterface::class);
        $app_variable->expects('name')->with('Deal ID')->once()->andReturnSelf();

        $this->registrar->expects('int')->withArgs(function (string $key, Closure $closure) use (&$closure_finished) {
            $this->assertEquals('dealId', $key);

            $context = Mockery::mock(RuntimeContext::class);

            $this->assertNull($closure($context));

            $closure_finished = true;

            return true;
        })->once()->andReturn($app_variable);

        $this->variables_registrar_injector->inject($variable);

        $this->assertTrue($closure_finished);
    }

    public function testInjectOnBaseVariableApiResolver(): void
    {
        $closure_finished = false;

        $variable = new Variable('dealId', 'int', 'Deal ID');
        $variable->resolver              = new VariableResolver(10);
        $variable->resolver->key         = 'deal_id';
        $variable->resolver->description = 'deal identity';

        $app_variable = Mockery::mock(VariableInterface::class);
        $app_variable->expects('name')->with('Deal ID')->once()->andReturnSelf();

        $this->registrar->expects('int')->withArgs(function (string $key, Closure $closure) use (&$closure_finished) {
            $this->assertEquals('dealId', $key);

            $context = Mockery::mock(RuntimeContext::class);
            $context->expects('cacheValue')->withArgs(function (string $key, Closure $context_closure) {
                $this->assertEquals('dealId', $key);
                $context_closure();

                return true;
            });

            $context->expects('chat->getExternalId')->once()->andReturn('1');
            $context->expects('appInstance->getHandler->getApiCommands->getChatVariableValue')->with('1', 'deal_id', 10)->once()->andReturn('ten');

            $closure($context);

            $closure_finished = true;

            return true;
        })->once()->andReturn($app_variable);

        $this->variables_registrar_injector->inject($variable);

        $this->assertTrue($closure_finished);
    }
}
