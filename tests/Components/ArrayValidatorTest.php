<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Components;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Components\ArrayValidator;
use BmPlatform\ApiModule\Tests\TestCase;

class ArrayValidatorTest extends TestCase
{
    public function testValidate(): void
    {
        $this->mockTranslatorIntoDI();

        ArrayValidator::validate(['a' => 'b'], ['a' => ['required', 'string']], 'test data');
    }

    public function testValidateOnFailure(): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => ArrayValidator::validate(['b' => 'b'], ['a' => ['required', 'string']], 'test data')
        );
    }
}
