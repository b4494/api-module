<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Components;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Components\ConfigValidator;
use BmPlatform\ApiModule\ServiceProvider;
use BmPlatform\ApiModule\Tests\TestCase;
use Illuminate\Config\Repository;
use Mockery;

class ConfigValidatorTest extends TestCase
{
    public function testValidate(): void
    {
        $this->mockTranslatorIntoDI();

        $config = Mockery::mock(Repository::class);
        $config->expects('get')->with(ServiceProvider::CONFIG_KEY, [])->once()->andReturn([
            'integrations' => [
                [
                    'code' => 'api1',
                    'url'  => 'https://example.com/api',
                    'key'  => 'super_secret'
                ],
                [
                    'code' => 'api2',
                    'url'  => 'https://example.com/api',
                    'key'  => 'super_secret'
                ]
            ]
        ]);

        ConfigValidator::validate($config);
    }

    public function testValidateOnNonUniqueCode(): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $config = Mockery::mock(Repository::class);
        $config->expects('get')->with(ServiceProvider::CONFIG_KEY, [])->once()->andReturn([
            'integrations' => [
                [
                    'code' => 'api',
                    'url'  => 'https://example.com/api',
                    'key'  => 'super_secret'
                ],
                [
                    'code' => 'api',
                    'url'  => 'https://example.com/api',
                    'key'  => 'super_secret'
                ]
            ]
        ]);

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => ConfigValidator::validate($config)
        );
    }

    public function testValidateOnMissedCodeAndKey(): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $config = Mockery::mock(Repository::class);
        $config->expects('get')->with(ServiceProvider::CONFIG_KEY, [])->once()->andReturn([
            'integrations' => [
                [
                    'url' => 'https://example.com/api',
                ]
            ]
        ]);

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => ConfigValidator::validate($config)
        );
    }
}
