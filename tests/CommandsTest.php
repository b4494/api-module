<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests;

use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Interfaces\Chat as ChatInterface;
use BmPlatform\Abstraction\Interfaces\Contact as ContactInterface;
use BmPlatform\Abstraction\Interfaces\Operator as OperatorInterface;
use BmPlatform\Abstraction\Interfaces\Tag as TagInterface;
use BmPlatform\Abstraction\Requests\ChatTagRequest;
use BmPlatform\Abstraction\Requests\SendMediaRequest;
use BmPlatform\Abstraction\Requests\SendSystemMessageRequest;
use BmPlatform\Abstraction\Requests\SendTextMessageRequest;
use BmPlatform\Abstraction\Requests\TransferChatToOperatorRequest;
use BmPlatform\Abstraction\Requests\UpdateContactDataRequest;
use BmPlatform\Abstraction\Responses\MessageSendResult;
use BmPlatform\Abstraction\Responses\NewOperatorResponse;
use BmPlatform\ApiModule\ApiIntegrationUserCommands;
use BmPlatform\ApiModule\Commands;
use Mockery;

class CommandsTest extends TestCase
{
    private Commands $commands;
    private ApiIntegrationUserCommands|Mockery\MockInterface $api_commands;

    public function setUp(): void
    {
        parent::setUp();

        $this->api_commands = Mockery::mock(ApiIntegrationUserCommands::class);
        $this->commands    = new Commands($this->api_commands);
    }

    public function tearDown(): void
    {
        parent::tearDown();

        unset($this->commands, $this->api_commands);
    }

    public function testSendTextMessage(): void
    {
        $request = new SendTextMessageRequest(
            chat: Mockery::mock(ChatInterface::class),
            text: 'hello!'
        );

        $response = new MessageSendResult(
            externalId: '1'
        );

        $this->api_commands
            ->expects('sendTextMessage')
            ->once()
            ->andReturn($response);

        $this->assertEquals($response, $this->commands->sendTextMessage($request));
    }

    public function testSendMediaMessage(): void
    {
        $request = new SendMediaRequest(
            chat: Mockery::mock(ChatInterface::class),
            file: new MediaFile(
                type: MediaFileType::Image,
                url: 'https://example.com/image.jpg'
            ),
        );

        $response = new MessageSendResult(
            externalId: '1'
        );

        $this->api_commands
            ->expects('sendMediaMessage')
            ->once()
            ->andReturn($response);

        $this->assertEquals($response, $this->commands->sendMediaMessage($request));
    }

    public function testSendSystemMessage(): void
    {
        $request = new SendSystemMessageRequest(
            chat: Mockery::mock(ChatInterface::class),
            text: 'hello!'
        );

        $response = new MessageSendResult(
            externalId: '1'
        );

        $this->api_commands
            ->expects('sendSystemMessage')
            ->once()
            ->andReturn($response);

        $this->commands->sendSystemMessage($request);
    }

    public function testAttachTagToChat(): void
    {
        $request = new ChatTagRequest(
            chat: Mockery::mock(ChatInterface::class),
            tag: Mockery::mock(TagInterface::class),
            forTicket: true
        );

        $this->api_commands
            ->expects('attachTagToChat')
            ->once();

        $this->commands->attachTagToChat($request);
    }

    public function testDetachTagFromChat(): void
    {
        $request = new ChatTagRequest(
            chat: Mockery::mock(ChatInterface::class),
            tag: Mockery::mock(TagInterface::class),
            forTicket: true
        );

        $this->api_commands
            ->expects('detachTagFromChat')
            ->once();

        $this->commands->detachTagFromChat($request);
    }

    public function testCloseChatTicket(): void
    {
        $request = Mockery::mock(ChatInterface::class);

        $this->api_commands
            ->expects('closeChatTicket')
            ->once();

        $this->commands->closeChatTicket($request);
    }

    public function testTransferChatToOperator(): void
    {
        $request = new TransferChatToOperatorRequest(
            chat: Mockery::mock(ChatInterface::class),
            target: Mockery::mock(OperatorInterface::class)
        );

        $response = new NewOperatorResponse(
            operator: '1'
        );

        $this->api_commands
            ->expects('transferChatToOperator')
            ->once()
            ->andReturn($response);

        $this->assertEquals($response, $this->commands->transferChatToOperator($request));
    }

    public function testUpdateContactData(): void
    {
        $request = new UpdateContactDataRequest(
            contact: Mockery::mock(ContactInterface::class)
        );

        $this->api_commands
            ->expects('updateContactData')
            ->once();

        $this->commands->updateContactData($request);
    }
}
