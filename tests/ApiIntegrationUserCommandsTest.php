<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests;

use BmPlatform\Abstraction\DataTypes\InlineButton;
use BmPlatform\Abstraction\DataTypes\InlineUrlButton;
use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Abstraction\DataTypes\OperatorGroup;
use BmPlatform\Abstraction\DataTypes\QuickReply;
use BmPlatform\Abstraction\DataTypes\Tag;
use BmPlatform\Abstraction\Enums\ButtonColor;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Enums\MessengerType;
use BmPlatform\Abstraction\Enums\QuickReplyType;
use BmPlatform\Abstraction\Enums\VariableCategory;
use BmPlatform\Abstraction\Interfaces\Chat as ChatInterface;
use BmPlatform\Abstraction\Interfaces\Contact as ContactInterface;
use BmPlatform\Abstraction\Interfaces\MessengerInstance as MessengerInstanceInterface;
use BmPlatform\Abstraction\Interfaces\Operator as OperatorInterface;
use BmPlatform\Abstraction\Interfaces\OperatorGroup as OperatorGroupInterface;
use BmPlatform\Abstraction\Interfaces\Tag as TagInterface;
use BmPlatform\Abstraction\Requests\ChatTagRequest;
use BmPlatform\Abstraction\Requests\SendMediaRequest;
use BmPlatform\Abstraction\Requests\SendSystemMessageRequest;
use BmPlatform\Abstraction\Requests\SendTextMessageRequest;
use BmPlatform\Abstraction\Requests\TransferChatToOperatorRequest;
use BmPlatform\Abstraction\Requests\UpdateContactDataRequest;
use BmPlatform\Abstraction\Responses\MessageSendResult;
use BmPlatform\Abstraction\Responses\NewOperatorResponse;
use BmPlatform\ApiModule\ApiClient;
use BmPlatform\ApiModule\ApiIntegrationUserCommands;
use BmPlatform\ApiModule\Entities\Integration;
use BmPlatform\ApiModule\Entities\Variables\ComplexVariable;
use BmPlatform\ApiModule\Entities\Variables\EnumVariable;
use BmPlatform\ApiModule\Entities\Variables\EnumVariableOption;
use BmPlatform\ApiModule\Entities\Variables\Variable;
use BmPlatform\ApiModule\Entities\Variables\VariableResolver;
use Illuminate\Support\Facades\App;
use Mockery;

class ApiIntegrationUserCommandsTest extends TestCase
{
    private ApiClient|Mockery\MockInterface $api_client;
    private ApiIntegrationUserCommands $api_commands;

    public function setUp(): void
    {
        parent::setUp();

        $this->api_client = Mockery::mock(ApiClient::class);

        App::expects('makeWith')->with(ApiClient::class, [
            'base_url'   => 'https://example.com/1',
            'secret_key' => 'something_secret'
        ])->once()->andReturn($this->api_client);

        $this->api_commands = new ApiIntegrationUserCommands(
            new Integration(
                code: 'api',
                url: 'https://example.com',
                key: 'something_secret'
            ),
            '1'
        );
    }

    public function tearDown(): void
    {
        parent::tearDown();

        unset($this->api_commands, $this->api_client);
    }

    public function testGetExtraVariables(): void
    {
        $this->mockTranslatorIntoDI();

        $response_data = [
            [
                'key'                => 'dealId',
                'type'               => 'int',
                'resolver'           => 'chat.extraData.dealId',
                'name'               => 'Deal ID',
                'category'           => 'shop',
                'array'              => true,
                'hideFromConditions' => true
            ],
            [
                'key'      => 'complexDeal',
                'type'     => 'complex',
                'resolver' => [
                    'description' => 'deal field',
                    'lifetime'    => 10,
                    'key'         => 'complexDealId'
                ],
                'name'               => 'Complex Deal',
                'category'           => 'shop',
                'hideFromConditions' => true,
                'properties'         => [
                    [
                        'key'                => 'id',
                        'type'               => 'int',
                        'resolver'           => 'chat.extraData.dealId',
                        'name'               => 'Deal ID',
                        'category'           => 'shop',
                        'hideFromConditions' => true
                    ],
                ]
            ],
            [
                'key'                => 'enumDeal',
                'type'               => 'enum',
                'name'               => 'string',
                'category'           => 'string',
                'hideFromConditions' => true,
                'options'            => [
                    [
                        'value' => 'deal_simple',
                        'label' => 'Simple Deal'
                    ],
                    [
                        'value' => 'deal_complex',
                        'label' => 'Complex Deal'
                    ]
                ]
            ]
        ];

        $this->api_client->expects('get')
            ->with('/extraVariables')
            ->once()
            ->andReturns($response_data);

        $base_variable = new Variable('dealId', 'int', 'Deal ID');
        $base_variable->resolver = 'chat.extraData.dealId';
        $base_variable->category = VariableCategory::shop;
        $base_variable->array    = true;
        $base_variable->hide_from_conditions = true;

        $complex_property_1 = new Variable('id', 'int', 'Deal ID');
        $complex_property_1->resolver = 'chat.extraData.dealId';
        $complex_property_1->category = VariableCategory::shop;
        $complex_property_1->hide_from_conditions = true;

        $complex_variable = new ComplexVariable('complexDeal', 'Complex Deal', [$complex_property_1]);
        $complex_variable->resolver = new VariableResolver(10);
        $complex_variable->resolver->description = 'deal field';
        $complex_variable->resolver->key = 'complexDealId';
        $complex_variable->category = VariableCategory::shop;
        $complex_variable->hide_from_conditions = true;

        $enum_variable = new EnumVariable('enumDeal', 'string', [
            new EnumVariableOption('Simple Deal', 'deal_simple'),
            new EnumVariableOption('Complex Deal', 'deal_complex'),
        ]);

        $enum_variable->resolver = null;
        $enum_variable->category = 'string';
        $enum_variable->hide_from_conditions = true;

        $this->assertEquals([
            $base_variable,
            $complex_variable,
            $enum_variable
        ], $this->api_commands->getExtraVariables());
    }

    public function testGetExtraVariablesOnNonCamelCaseKey(): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $response_data = [
            [
                'key'                => 'deal_id',
                'type'               => 'int',
                'resolver'           => 'chat.extraData.dealId',
                'name'               => 'Deal ID',
                'category'           => 'shop',
                'hideFromConditions' => true
            ],
        ];

        $this->api_client->expects('get')
            ->with('/extraVariables')
            ->once()
            ->andReturn($response_data);

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => $this->api_commands->getExtraVariables()
        );
    }

    public function testGetExtraVariablesOnMissedEnumOptions(): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $response_data = [
            [
                'key'                => 'enumDeal',
                'type'               => 'enum',
                'name'               => 'string',
                'category'           => 'string',
                'hideFromConditions' => true,
            ],
        ];

        $this->api_client->expects('get')
            ->with('/extraVariables')
            ->once()
            ->andReturn($response_data);

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => $this->api_commands->getExtraVariables()
        );
    }

    public function testGetMessengers(): void
    {
        $this->mockTranslatorIntoDI();

        $this->api_client->expects('get')
            ->with('/messengers', ['cursor' => null])
            ->once()
            ->andReturn([
                'items' => [
                    [
                        'externalId'   => '1',
                        'externalType' => 'twitter',
                        'name'         => 'Twitter Bot',
                        'description'  => 'Twitter Welcome Bot',
                        'messengerId'  => 'twitter_welcome_bot',
                        'internalType' => null
                    ],
                    [
                        'externalId'   => '2',
                        'externalType' => 'instagram',
                        'name'         => 'Instagram Direct',
                        'internalType' => MessengerType::Instagram->name
                    ]
                ],
                'cursor' => '50'
            ]);

        $expected = new IterableData([
            new MessengerInstance(
                externalType: 'twitter',
                externalId: '1',
                name: 'Twitter Bot',
                description: 'Twitter Welcome Bot',
                messengerId: 'twitter_welcome_bot',
            ),
            new MessengerInstance(
                externalType: 'instagram',
                externalId: '2',
                name: 'Instagram Direct',
                internalType: MessengerType::Instagram
            )
        ], '50');

        $this->assertEquals($expected, $this->api_commands->getMessengers());
    }

    public function testGetMessengersOnIncorrectInternalType(): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->api_client->expects('get')
            ->with('/messengers', ['cursor' => null])
            ->once()
            ->andReturn([
                'items' => [
                    [
                        'externalType' => 'twitter',
                        'name'         => 'Twitter Bot',
                        'internalType' => 'twitter_bot_incorrect_type'
                    ],
                ],
                'cursor' => '50'
            ]);

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => $this->api_commands->getMessengers()
        );
    }

    public function testGetTags(): void
    {
        $this->mockTranslatorIntoDI();

        $this->api_client->expects('get')
            ->with('/tags', ['cursor' => null])
            ->once()
            ->andReturn([
                'items' => [
                    [
                        'externalId' => '1',
                        'label'      => 'tag',
                        'group'      => 'group'
                    ],
                ],
            ]);

        $expected = new IterableData([
            new Tag(
                externalId: '1',
                label: 'tag',
                group: 'group'
            ),
        ], null);

        $this->assertEquals($expected, $this->api_commands->getTags());
    }

    public function testGetTagsOnMissedLabel(): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->api_client->expects('get')
            ->with('/tags', ['cursor' => null])
            ->once()
            ->andReturn([
                'items' => [
                    [
                        'externalId' => '1'
                    ],
                ],
                'cursor' => null
            ]);

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => $this->api_commands->getTags()
        );
    }

    public function testGetOperators(): void
    {
        $this->mockTranslatorIntoDI()->expects('get')->zeroOrMoreTimes()->andReturn('invalid');

        $this->api_client->expects('get')
            ->with('/operators', ['cursor' => null])
            ->once()
            ->andReturn([
                'items' => [
                    [
                        'externalId' => '1',
                        'name'       => 'Tank',
                        'phone'      => '+79998887766',
                        'email'      => 'operator@example.com',
                        'avatarUrl'  => 'https://media.zenfs.com/en/nerdist_761/2d47d0794ed390d7807134077817ca40',
                        'extraData'  => [
                            'nothing_special'
                        ]
                    ],
                ],
            ]);

        $expected = new IterableData([
            new Operator(
                externalId: '1',
                name: 'Tank',
                phone: '+79998887766',
                email: 'operator@example.com',
                avatarUrl: 'https://media.zenfs.com/en/nerdist_761/2d47d0794ed390d7807134077817ca40',
                extraData: [
                    'nothing_special'
                ]
            ),
        ], null);

        $this->assertEquals($expected, $this->api_commands->getOperators());
    }

    public function testGetOperatorsOnNameMissed(): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->api_client->expects('get')
            ->with('/operators', ['cursor' => null])
            ->once()
            ->andReturn([
                'items' => [
                    [
                        'externalId' => '1'
                    ],
                ],
                'cursor' => null
            ]);

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => $this->api_commands->getOperators()
        );
    }

    public function testGetOperatorGroups(): void
    {
        $this->mockTranslatorIntoDI();

        $this->api_client->expects('get')
            ->with('/operatorGroups', ['cursor' => null])
            ->once()
            ->andReturn([
                'items' => [
                    [
                        'externalId' => '1',
                        'name'       => 'group',
                        'extraData'  => [
                            'nothing_special'
                        ]
                    ],
                ],
            ]);

        $expected = new IterableData([
            new OperatorGroup(
                externalId: '1',
                name: 'group',
                extraData: [
                    'nothing_special'
                ]
            ),
        ], null);

        $this->assertEquals($expected, $this->api_commands->getOperatorGroups());
    }

    public function testGetOperatorGroupsOnExternalIdMissed(): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->api_client->expects('get')
            ->with('/operatorGroups', ['cursor' => null])
            ->once()
            ->andReturn([
                'items' => [
                    [
                        'name' => 'group'
                    ],
                ],
                'cursor' => null
            ]);

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => $this->api_commands->getOperatorGroups()
        );
    }

    public function testGetChatVariableValue(): void
    {
        $this->mockTranslatorIntoDI();

        $this->api_client->expects('getCached')
            ->with('/chats/ten/variables/dialId', [], 1)
            ->once()
            ->andReturn([
                'value' => 'eleven'
            ]);

        $this->assertEquals('eleven', $this->api_commands->getChatVariableValue('ten', 'dialId', 1));
    }

    public function testSendTextMessage(): void
    {
        $this->mockTranslatorIntoDI();

        $this->api_client
            ->expects('post')
            ->withArgs(function (string $url, array $args) {
                $this->assertEquals('/sendTextMessage', $url);
                $this->assertEquals([
                    'chat'          => $this->getChatPartnerRequestData(),
                    'quickReplies'  => $this->getQuickRepliesPartnerRequestData(),
                    'inlineButtons' => $this->getInlineButtonsPartnerRequestData(),
                    'text'          => 'hi!'
                ], $args);

                return true;
            })
            ->once()
            ->andReturn([
                'externalId'  => '1',
                'messengerId' => 'twitter_welcome_bot'
            ]);

        $request = new SendTextMessageRequest(
            chat: $this->getMockedChatInterface(),
            text: 'hi!',
            quickReplies: $this->getQuickRepliesInternalRequestData(),
            inlineButtons: $this->getInlineButtonsInternalRequestData()
        );

        $expected = new MessageSendResult(
            externalId: '1',
            messengerId: 'twitter_welcome_bot'
        );

        $this->assertEquals($expected, $this->api_commands->sendTextMessage($request));
    }

    public function testSendTextMessageOnIncorrectResponse(): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->api_client
            ->expects('post')
            ->withArgs(function (string $url, array $args) {
                $this->assertEquals('/sendTextMessage', $url);
                $this->assertEquals([
                    'chat'          => $this->getChatPartnerRequestData(),
                    'quickReplies'  => $this->getQuickRepliesPartnerRequestData(),
                    'inlineButtons' => $this->getInlineButtonsPartnerRequestData(),
                    'text'          => 'hi!'
                ], $args);

                return true;
            })
            ->once()
            ->andReturn([
                'message' => 'success',
                'code'    => 200
            ]);

        $request = new SendTextMessageRequest(
            chat: $this->getMockedChatInterface(),
            text: 'hi!',
            quickReplies: $this->getQuickRepliesInternalRequestData(),
            inlineButtons: $this->getInlineButtonsInternalRequestData()
        );

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => $this->api_commands->sendTextMessage($request)
        );
    }

    public function testSendMediaMessage(): void
    {
        $this->mockTranslatorIntoDI();

        $this->api_client
            ->expects('post')
            ->withArgs(function (string $url, array $args) {
                $this->assertEquals('/sendMediaMessage', $url);
                $this->assertEquals([
                    'chat'          => $this->getChatPartnerRequestData(),
                    'quickReplies'  => $this->getQuickRepliesPartnerRequestData(),
                    'inlineButtons' => $this->getInlineButtonsPartnerRequestData(),
                    'file'          => [
                        'type' => 'Image',
                        'url'  => 'https://example.com/image.jpg',
                        'mime' => 'image/jpeg'
                    ],
                    'caption' => 'hi!'
                ], $args);

                return true;
            })
            ->once()
            ->andReturn([
                'externalId'  => '1',
                'messengerId' => 'twitter_welcome_bot'
            ]);

        $request = new SendMediaRequest(
            chat: $this->getMockedChatInterface(),
            file:new MediaFile(
                type: MediaFileType::Image,
                url: 'https://example.com/image.jpg',
                mime:'image/jpeg'
            ),
            caption: 'hi!',
            quickReplies: $this->getQuickRepliesInternalRequestData(),
            inlineButtons: $this->getInlineButtonsInternalRequestData()
        );

        $expected = new MessageSendResult(
            externalId: '1',
            messengerId: 'twitter_welcome_bot'
        );

        $this->assertEquals($expected, $this->api_commands->sendMediaMessage($request));
    }

    public function testSendSystemMessage(): void
    {
        $this->mockTranslatorIntoDI();

        $this->api_client
            ->expects('post')
            ->withArgs(function (string $url, array $args) {
                $this->assertEquals('/sendSystemMessage', $url);
                $this->assertEquals([
                    'chat' => $this->getChatPartnerRequestData(),
                    'text' => 'hi!'
                ], $args);

                return true;
            })
            ->once()
            ->andReturn([
                'externalId'  => '1',
                'messengerId' => 'twitter_welcome_bot'
            ]);

        $request = new SendSystemMessageRequest(
            chat: $this->getMockedChatInterface(),
            text: 'hi!'
        );

        $expected = new MessageSendResult(
            externalId: '1',
            messengerId: 'twitter_welcome_bot'
        );

        $this->assertEquals($expected, $this->api_commands->sendSystemMessage($request));
    }

    public function testAttachTagToChat(): void
    {
        $this->api_client
            ->expects('post')
            ->withArgs(function (string $url, array $args) {
                $this->assertEquals('/attachTagToChat', $url);
                $this->assertEquals([
                    'chat'          => $this->getChatPartnerRequestData(),
                    'tagExternalId' => '10',
                    'forTicket'     => true
                ], $args);

                return true;
            })
            ->once()
            ->andReturn([
                'message' => 'success',
                'code'    => 200
            ]);

        $request = new ChatTagRequest(
            chat: $this->getMockedChatInterface(),
            tag: $this->getMockedTagInterface(),
            forTicket: true
        );

        $this->api_commands->attachTagToChat($request);
    }

    public function testDetachTagToChat(): void
    {
        $this->api_client
            ->expects('post')
            ->withArgs(function (string $url, array $args) {
                $this->assertEquals('/detachTagFromChat', $url);
                $this->assertEquals([
                    'chat'          => $this->getChatPartnerRequestData(),
                    'tagExternalId' => '10',
                    'forTicket'     => true
                ], $args);

                return true;
            })
            ->once()
            ->andReturn([
                'message' => 'success',
                'code'    => 200
            ]);

        $request = new ChatTagRequest(
            chat: $this->getMockedChatInterface(),
            tag: $this->getMockedTagInterface(),
            forTicket: true
        );

        $this->api_commands->detachTagFromChat($request);
    }

    public function testCloseChatTicket(): void
    {
        $this->api_client
            ->expects('post')
            ->withArgs(function (string $url, array $args) {
                $this->assertEquals('/closeChatTicket', $url);
                $this->assertEquals([
                    'chat' => $this->getChatPartnerRequestData(),
                ], $args);

                return true;
            })
            ->once()
            ->andReturn([
                'message' => 'success',
                'code'    => 200
            ]);

        $this->api_commands->closeChatTicket($this->getMockedChatInterface());
    }

    public function testTransferChatToOperatorOnTargetOperator(): void
    {
        $this->mockTranslatorIntoDI();

        $this->api_client
            ->expects('post')
            ->withArgs(function (string $url, array $args) {
                $this->assertEquals('/transferChatToOperator', $url);
                $this->assertEquals([
                    'targetExternalId' => '10',
                    'targetType'       => 'operator',
                    'messengerId'      => 'twitter_welcome_bot'
                ], $args);

                return true;
            })
            ->once()
            ->andReturn([
                'operator'    => '10',
                'messengerId' => 'twitter_welcome_bot'
            ]);

        $operator = Mockery::mock(OperatorInterface::class);
        $operator->expects('getExternalId')->once()->andReturn('10');

        $request = new TransferChatToOperatorRequest(
            chat: $this->getMockedChatInterface(),
            target: $operator
        );

        $expected = new NewOperatorResponse(
            operator: '10'
        );

        $this->assertEquals($expected, $this->api_commands->transferChatToOperator($request));
    }

    public function testTransferChatToOperatorOnTargetOperatorAndIncorrectResponse(): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->api_client
            ->expects('post')
            ->withArgs(function (string $url, array $args) {
                $this->assertEquals('/transferChatToOperator', $url);
                $this->assertEquals([
                    'targetExternalId' => '10',
                    'targetType'       => 'operator',
                    'messengerId'      => 'twitter_welcome_bot'
                ], $args);

                return true;
            })
            ->once()
            ->andReturn([
                'code'        => '200',
                'messengerId' => 'success'
            ]);

        $operator = Mockery::mock(OperatorInterface::class);
        $operator->expects('getExternalId')->once()->andReturn('10');

        $request = new TransferChatToOperatorRequest(
            chat: $this->getMockedChatInterface(),
            target: $operator
        );

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => $this->api_commands->transferChatToOperator($request)
        );
    }

    public function testTransferChatToOperatorOnTargetOperatorGroup(): void
    {
        $this->mockTranslatorIntoDI();

        $this->api_client
            ->expects('post')
            ->withArgs(function (string $url, array $args) {
                $this->assertEquals('/transferChatToOperator', $url);
                $this->assertEquals([
                    'targetExternalId' => 'super_group',
                    'targetType'       => 'operatorGroup',
                    'messengerId'      => 'twitter_welcome_bot'
                ], $args);

                return true;
            })
            ->once()
            ->andReturn([
                'operator'    => 'super_group',
                'messengerId' => 'twitter_welcome_bot'
            ]);

        $operator = Mockery::mock(OperatorGroupInterface::class);
        $operator->expects('getName')->once()->andReturn('super_group');

        $request = new TransferChatToOperatorRequest(
            chat: $this->getMockedChatInterface(),
            target: $operator
        );

        $expected = new NewOperatorResponse(
            operator: 'super_group'
        );

        $this->assertEquals($expected, $this->api_commands->transferChatToOperator($request));
    }

    public function testUpdateContactData(): void
    {
        $this->api_client
            ->expects('post')
            ->withArgs(function (string $url, array $args) {
                $this->assertEquals('/updateContactData', $url);
                $this->assertEquals(
                    [
                        'externalId'  => '10',
                        'name'        => 'Contact',
                        'phone'       => '+79998887744',
                        'email'       => 'example@example.com',
                        'avatarUrl'   => 'https://example.com/photo.jpg',
                        'extraFields' => [
                            'pat_name' => 'Boris'
                        ],
                        'messengerId' => 'idk',
                        'extraData'   => [
                            'bio' => 'random person'
                        ]
                    ],
                    $args
                );

                return true;
            })
            ->once()
            ->andReturn([
                'message' => 'success',
                'code'    => 200
            ]);

        $contact = Mockery::mock(ContactInterface::class);
        $contact->expects('getExternalId')->once()->andReturn('10');
        $contact->expects('getAvatarUrl')->once()->andReturn('https://example.com/photo.jpg');
        $contact->expects('getMessengerId')->once()->andReturn('idk');
        $contact->expects('getExtraData')->once()->andReturn(['bio' => 'random person']);

        $request = new UpdateContactDataRequest(
            contact: $contact,
            name: 'Contact',
            comment: 'Comment',
            phone: '+79998887744',
            email: 'example@example.com',
            fields: [
                'pat_name' => 'Boris'
            ],
        );

        $this->api_commands->updateContactData($request);
    }

    private function getMockedChatInterface(): Mockery\MockInterface|ChatInterface
    {
        $contact = Mockery::mock(ContactInterface::class);
        $contact->expects('getExternalId')->zeroOrMoreTimes()->andReturn('15');

        $operator = Mockery::mock(OperatorInterface::class);
        $operator->expects('getExternalId')->zeroOrMoreTimes()->andReturn('18');

        $messenger_instance = Mockery::mock(MessengerInstanceInterface::class);
        $messenger_instance->expects('getExternalId')->zeroOrMoreTimes()->andReturn('12');

        $chat = Mockery::mock(ChatInterface::class);
        $chat->expects('getExternalId')->zeroOrMoreTimes()->andReturn('10');
        $chat->expects('getMessengerInstance')->zeroOrMoreTimes()->andReturn($messenger_instance);
        $chat->expects('getContact')->zeroOrMoreTimes()->andReturn($contact);
        $chat->expects('getOperator')->zeroOrMoreTimes()->andReturn($operator);
        $chat->expects('getMessengerId')->zeroOrMoreTimes()->andReturn('twitter_welcome_bot');
        $chat->expects('getExtraData')->zeroOrMoreTimes()->andReturn(['not_so_extra']);

        return $chat;
    }

    private function getMockedTagInterface(): Mockery\MockInterface|ChatInterface
    {
        $tag = Mockery::mock(TagInterface::class);
        $tag->expects('getExternalId')->once()->andReturn('10');

        return $tag;
    }

    private function getChatPartnerRequestData(): array
    {
        return [
            'externalId'        => '10',
            'messengerInstance' => '12',
            'contact'           => '15',
            'operator'          => '18',
            'messengerId'       => 'twitter_welcome_bot',
            'extraData'         => ['not_so_extra']
        ];
    }

    /**
     * @return QuickReply[][]
     */
    private function getQuickRepliesInternalRequestData(): array
    {
        return [
            [
                new QuickReply(text: 'reply', type: QuickReplyType::Text, color: ButtonColor::Secondary),
                new QuickReply(text: 'phone', type: QuickReplyType::Phone, color: ButtonColor::Primary),
            ],
        ];
    }

    /**
     * @return InlineUrlButton[][]|InlineButton[][]
     */
    private function getInlineButtonsInternalRequestData(): array
    {
        return [
            [
                new InlineUrlButton(text: 'url', url: 'https://example.com', payload: 'Push Me!'),
                new InlineButton(text: 'reply', payload: 'Don\'t push me!', color: ButtonColor::Negative),
            ]
        ];
    }

    private function getQuickRepliesPartnerRequestData(): array
    {
        return [
            [
                [
                    'text'  => 'reply',
                    'type'  => QuickReplyType::Text->name,
                    'color' => ButtonColor::Secondary->name,
                ],
                [
                    'text'  => 'phone',
                    'type'  => QuickReplyType::Phone->name,
                    'color' => ButtonColor::Primary->name,
                ]
            ],
        ];
    }

    private function getInlineButtonsPartnerRequestData(): array
    {
        return [
            [
                [
                    'text'    => 'url',
                    'payload' => 'Push Me!',
                    'color'   => null,
                    'url'     => 'https://example.com'
                ],
                [
                    'text'    => 'reply',
                    'payload' => 'Don\'t push me!',
                    'color'   => ButtonColor::Negative->name,
                    'url'     => null
                ]
            ]
        ];
    }
}
