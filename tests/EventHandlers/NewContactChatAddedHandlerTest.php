<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\EventHandlers;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\Events\NewContactChatAdded;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\NewContactChatAddedHandlerInput;
use BmPlatform\ApiModule\EventHandlers\NewContactChatAddedHandler;
use Carbon\Carbon;

class NewContactChatAddedHandlerTest extends AbstractEventHandlerTestCase
{
    public function testInvoke(): void
    {
        $input = new NewContactChatAddedHandlerInput([
            'chat' => [
                'externalId'        => '10',
                'messengerInstance' => '20',
                'contact'           => '30',
                'operator'          => '40',
                'messengerId'       => '50',
                'extraData'         => [
                    'key' => 'value'
                ]
            ],
            'contact' => [
                'externalId'  => '10',
                'name'        => 'Contact',
                'phone'       => '+79995553322',
                'avatarUrl'   => 'https://example.com/image.jpg',
                'extraFields' => [
                    'extra' => 'fields'
                ],
                'messengerId' => 'contact_handle',
                'extraData'   => [
                    'extra' => 'data'
                ],
            ],
            'extraData' => [
                'extra' => 'data'
            ],
            'timestamp' => '1577836800'
        ]);

        $expected = new NewContactChatAdded(
            chat: new Chat(
                externalId: '10',
                messengerInstance: '20',
                contact: '30',
                operator: '40',
                messengerId: '50',
                extraData: [
                    'key' => 'value'
                ]
            ),
            contact: new Contact(
                externalId: '10',
                name: 'Contact',
                phone: '+79995553322',
                avatarUrl: 'https://example.com/image.jpg',
                extraFields: [
                    'extra' => 'fields'
                ],
                messengerId: 'contact_handle',
                extraData: [
                    'extra' => 'data'
                ],
            ),
            extraData: [
                'extra' => 'data'
            ],
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new NewContactChatAddedHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }

    public function testInvokeOnMinData(): void
    {
        $input = new NewContactChatAddedHandlerInput([
            'chat' => [
                'externalId'        => '10',
                'messengerInstance' => '20'
            ],
            'contact' => [
                'externalId' => '10',
            ],
            'timestamp' => '1577836800'
        ]);

        $expected = new NewContactChatAdded(
            chat: new Chat(
                externalId: '10',
                messengerInstance: '20',
            ),
            contact: new Contact(
                externalId: '10',
            ),
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new NewContactChatAddedHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }
}
