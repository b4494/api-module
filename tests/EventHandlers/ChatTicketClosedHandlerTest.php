<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\EventHandlers;

use BmPlatform\Abstraction\Events\ChatTicketClosed;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ChatTicketClosedHandlerInput;
use BmPlatform\ApiModule\EventHandlers\ChatTicketClosedHandler;
use Carbon\Carbon;

class ChatTicketClosedHandlerTest extends AbstractEventHandlerTestCase
{
    public function testInvoke(): void
    {
        $input = new ChatTicketClosedHandlerInput([
            'chat'      => '10',
            'ticketId'  => '20',
            'timestamp' => '1577836800'
        ]);

        $expected = new ChatTicketClosed(
            chat: '10',
            ticketId: '20',
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new ChatTicketClosedHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }
}
