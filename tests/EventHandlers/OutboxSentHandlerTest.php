<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\EventHandlers;

use BmPlatform\Abstraction\DataTypes\GeoLocation;
use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Events\OutboxSent;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\OutboxSentHandlerInput;
use BmPlatform\ApiModule\EventHandlers\OutboxSentHandler;
use Carbon\Carbon;

class OutboxSentHandlerTest extends AbstractEventHandlerTestCase
{
    public function testInvoke(): void
    {
        $input = new OutboxSentHandlerInput([
            'chat'    => '10',
            'message' => [
                'externalId'  => '30',
                'text'        => 'hola',
                'attachments' => [
                    [
                        'type' => 'Image',
                        'url'  => 'https://example.com/image.jpg',
                        'mime' => 'image/jpeg'
                    ]
                ],
                'date'      => '2020-01-01 00:00:01',
                'extraData' => [
                    'extra' => 'data'
                ],
                'geoLocation' => [
                    'lat'  => '10',
                    'long' => '20'
                ]
            ],
            'operator'  => '40',
            'timestamp' => '1577836800'
        ]);

        $expected = new OutboxSent(
            chat: '10',
            message: new MessageData(
                externalId: '30',
                text: 'hola',
                attachments: [
                    new MediaFile(
                        type: MediaFileType::Image,
                        url: 'https://example.com/image.jpg',
                        mime: 'image/jpeg',
                    )
                ],
                geoLocation: new GeoLocation('10', '20'),
                date: Carbon::parse('2020-01-01 00:00:01'),
                extraData: [
                    'extra' => 'data'
                ]
            ),
            operator: '40',
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new OutboxSentHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }

    public function testInvokeOnMinData(): void
    {
        $input = new OutboxSentHandlerInput([
            'chat'    => '10',
            'message' => [
                'externalId' => '30',
                'text'       => 'hola',
            ],
            'operator'  => '40',
            'timestamp' => '1577836800'
        ]);

        $expected = new OutboxSent(
            chat: '10',
            message: new MessageData(
                externalId: '30',
                text: 'hola',
            ),
            operator: '40',
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new OutboxSentHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }
}
