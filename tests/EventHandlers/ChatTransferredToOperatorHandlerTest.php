<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\EventHandlers;

use BmPlatform\Abstraction\Events\ChatTransferredToOperator;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ChatTransferredToOperatorHandlerInput;
use BmPlatform\ApiModule\EventHandlers\ChatTransferredToOperatorHandler;
use Illuminate\Support\Carbon;

class ChatTransferredToOperatorHandlerTest extends AbstractEventHandlerTestCase
{
    public function testInvoke(): void
    {
        $input = new ChatTransferredToOperatorHandlerInput([
            'chat'         => '10',
            'newOperator'  => '20',
            'prevOperator' => '30',
            'timestamp'    => '1577836800'
        ]);

        $expected = new ChatTransferredToOperator(
            chat: '10',
            newOperator: '20',
            prevOperator: '30',
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new ChatTransferredToOperatorHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }
}
