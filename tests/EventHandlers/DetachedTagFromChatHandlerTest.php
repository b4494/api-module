<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\EventHandlers;

use BmPlatform\Abstraction\Events\DetachedTagFromChat;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\DetachedTagFromChatHandlerInput;
use BmPlatform\ApiModule\EventHandlers\DetachedTagFromChatHandler;
use Carbon\Carbon;

class DetachedTagFromChatHandlerTest extends AbstractEventHandlerTestCase
{
    public function testInvoke(): void
    {
        $input = new DetachedTagFromChatHandlerInput([
            'chat'      => '10',
            'tag'       => '20',
            'forTicket' => true,
            'timestamp' => '1577836800'
        ]);

        $expected = new DetachedTagFromChat(
            chat: '10',
            tag: '20',
            forTicket: true,
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new DetachedTagFromChatHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }
}
