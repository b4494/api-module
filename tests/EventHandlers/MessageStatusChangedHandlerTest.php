<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\EventHandlers;

use BmPlatform\Abstraction\Enums\MessageStatus;
use BmPlatform\Abstraction\Events\MessageStatusChanged;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\MessageStatusChangedHandlerInput;
use BmPlatform\ApiModule\EventHandlers\MessageStatusChangedHandler;
use Carbon\Carbon;

class MessageStatusChangedHandlerTest extends AbstractEventHandlerTestCase
{
    public function testInvoke(): void
    {
        $input = new MessageStatusChangedHandlerInput([
            'chat'       => '10',
            'externalId' => '20',
            'status'     => 'Sent',
            'timestamp'  => '1577836800'
        ]);

        $expected = new MessageStatusChanged(
            chat: '10',
            externalId: '20',
            status: MessageStatus::Sent,
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new MessageStatusChangedHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }
}
