<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\EventHandlers;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\Events\ContactDataUpdated;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ContactDataUpdatedHandlerInput;
use BmPlatform\ApiModule\EventHandlers\ContactDataUpdatedHandler;
use Carbon\Carbon;

class ContactDataUpdatedHandlerTest extends AbstractEventHandlerTestCase
{
    public function testInvoke(): void
    {
        $input = new ContactDataUpdatedHandlerInput([
            'contact' => [
                'externalId'  => '10',
                'name'        => 'Contact',
                'phone'       => '+79995553322',
                'avatarUrl'   => 'https://example.com/image.jpg',
                'extraFields' => [
                    'extra' => 'fields'
                ],
                'messengerId' => 'contact_handle',
                'extraData'   => [
                    'extra' => 'data'
                ],
            ],
            'timestamp' => '1577836800'
        ]);

        $expected = new ContactDataUpdated(
            contact: new Contact(
                externalId: '10',
                name: 'Contact',
                phone: '+79995553322',
                avatarUrl: 'https://example.com/image.jpg',
                extraFields: [
                    'extra' => 'fields'
                ],
                messengerId: 'contact_handle',
                extraData: [
                    'extra' => 'data'
                ],
            ),
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new ContactDataUpdatedHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }

    public function testInvokeOnMinData(): void
    {
        $input = new ContactDataUpdatedHandlerInput([
            'contact' => [
                'externalId' => '10',
            ],
            'timestamp' => '1577836800'
        ]);

        $expected = new ContactDataUpdated(
            contact: new Contact(
                externalId: '10',
            ),
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new ContactDataUpdatedHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }
}
