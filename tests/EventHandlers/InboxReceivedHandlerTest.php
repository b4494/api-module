<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\EventHandlers;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\DataTypes\ExternalPlatformItem;
use BmPlatform\Abstraction\DataTypes\GeoLocation;
use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Abstraction\Enums\InboxFlags;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Events\InboxReceived;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\InboxReceivedHandlerInput;
use BmPlatform\ApiModule\EventHandlers\InboxReceivedHandler;
use Carbon\Carbon;

class InboxReceivedHandlerTest extends AbstractEventHandlerTestCase
{
    public function testInvoke(): void
    {
        $input = new InboxReceivedHandlerInput([
            'chat'        => '10',
            'participant' => '20',
            'message'     => [
                'externalId'  => '30',
                'text'        => 'hola',
                'attachments' => [
                    [
                        'type' => 'Image',
                        'url'  => 'https://example.com/image.jpg',
                        'mime' => 'image/jpeg'
                    ]
                ],
                'date'      => '2020-01-01 00:00:01',
                'extraData' => [
                    'extra' => 'data'
                ],
                'geoLocation' => [
                    'lat'  => '10',
                    'long' => '20'
                ]
            ],
            'externalItem' => [
                'externalId'   => '40',
                'url'          => 'https://example.com/image2.jpg',
                'title'        => 'title',
                'externalData' => [
                    'external' => 'data'
                ]
            ],
            'flags' => [
                'newTicketOpened'     => true,
                'newChatCreated'      => false,
                'externalPostComment' => true
            ],
            'timestamp' => '1577836800'
        ]);

        $expected = new InboxReceived(
            chat: '10',
            participant: '20',
            message: new MessageData(
                externalId: '30',
                text: 'hola',
                attachments: [
                    new MediaFile(
                        type: MediaFileType::Image,
                        url: 'https://example.com/image.jpg',
                        mime: 'image/jpeg',
                    )
                ],
                geoLocation: new GeoLocation('10', '20'),
                date: Carbon::parse('2020-01-01 00:00:01'),
                extraData: [
                    'extra' => 'data'
                ]
            ),
            externalItem: new ExternalPlatformItem(
                externalId: '40',
                url: 'https://example.com/image2.jpg',
                title: 'title',
                extraData: [
                    'external' => 'data'
                ]
            ),
            flags: InboxFlags::EXTERNAL_POST_COMMENT | InboxFlags::NEW_TICKET_OPENED,
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new InboxReceivedHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }

    public function testInvokeOnMinData(): void
    {
        $input = new InboxReceivedHandlerInput([
            'chat'        => '10',
            'participant' => '20',
            'message'     => [
                'externalId' => '30',
                'text'       => 'hola',
            ],
            'timestamp' => '1577836800'
        ]);

        $expected = new InboxReceived(
            chat: '10',
            participant: '20',
            message: new MessageData(
                externalId: '30',
                text: 'hola',
            ),
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new InboxReceivedHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }

    public function testInvokeOnFullStructures(): void
    {
        $input = new InboxReceivedHandlerInput([
            'chat' => [
                'externalId'        => '10',
                'messengerInstance' => [
                    'externalId'   => '1',
                    'externalType' => 'twitter',
                    'name'         => 'Twitter Bot',
                    'description'  => 'Twitter Welcome Bot',
                    'messengerId'  => 'twitter_welcome_bot',
                    'internalType' => null
                ],
                'contact' => [
                    'externalId'  => '10',
                    'name'        => 'Contact',
                    'phone'       => '+79995553322',
                    'avatarUrl'   => 'https://example.com/image.jpg',
                    'extraFields' => [
                        'extra' => 'fields'
                    ],
                    'messengerId' => 'contact_handle',
                    'extraData'   => [
                        'extra' => 'data'
                    ],
                ],
                'operator' => [
                    'externalId' => '1',
                    'name'       => 'Operator',
                    'phone'      => '+79998887766',
                    'email'      => 'operator@example.com',
                    'avatarUrl'  => 'https://example.com/image.jpg',
                    'extraData'  => [
                        'nothing_special'
                    ]
                ],
                'messengerId' => 'twitter_welcome_bot',
                'extraData'   => ['not_so_extra']
            ],
            'participant' => [
                'externalId'  => '10',
                'name'        => 'Contact',
                'phone'       => '+79995553322',
                'avatarUrl'   => 'https://example.com/image.jpg',
                'extraFields' => [
                    'extra' => 'fields'
                ],
                'messengerId' => 'contact_handle',
                'extraData'   => [
                    'extra' => 'data'
                ],
            ],
            'message' => [
                'externalId' => '30',
                'text'       => 'hola',
            ],
            'timestamp' => '1577836800'
        ]);

        $expected = new InboxReceived(
            chat: new Chat(
                externalId: '10',
                messengerInstance: new MessengerInstance(
                    externalType: 'twitter',
                    externalId: '1',
                    name: 'Twitter Bot',
                    description: 'Twitter Welcome Bot',
                    messengerId: 'twitter_welcome_bot',
                    internalType: null
                ),
                contact: new Contact(
                    externalId: '10',
                    name: 'Contact',
                    phone: '+79995553322',
                    avatarUrl: 'https://example.com/image.jpg',
                    extraFields: [
                        'extra' => 'fields'
                    ],
                    messengerId: 'contact_handle',
                    extraData: [
                        'extra' => 'data'
                    ],
                ),
                operator: new Operator(
                    externalId: '1',
                    name: 'Operator',
                    phone: '+79998887766',
                    email: 'operator@example.com',
                    avatarUrl: 'https://example.com/image.jpg',
                    extraData: [
                        'nothing_special'
                    ]
                ),
                messengerId: 'twitter_welcome_bot',
                extraData: [
                    'not_so_extra'
                ]
            ),
            participant: new Contact(
                externalId: '10',
                name: 'Contact',
                phone: '+79995553322',
                avatarUrl: 'https://example.com/image.jpg',
                extraFields: [
                    'extra' => 'fields'
                ],
                messengerId: 'contact_handle',
                extraData: [
                    'extra' => 'data'
                ],
            ),
            message: new MessageData(
                externalId: '30',
                text: 'hola',
            ),
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new InboxReceivedHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }
}
