<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\EventHandlers;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\Events\ChatDataChanged;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ChatDataChangedHandlerInput;
use BmPlatform\ApiModule\EventHandlers\ChatDataChangedHandler;
use Carbon\Carbon;

class ChatDataChangedHandlerTest extends AbstractEventHandlerTestCase
{
    public function testInvoke(): void
    {
        $input = new ChatDataChangedHandlerInput([
            'chat' => [
                'externalId'        => '10',
                'messengerInstance' => '20',
                'contact'           => '30',
                'operator'          => '40',
                'messengerId'       => '50',
                'extraData'         => [
                    'key' => 'value'
                ]
            ],
            'timestamp' => '1577836800'
        ]);

        $expected = new ChatDataChanged(
            chat: new Chat(
                externalId: '10',
                messengerInstance: '20',
                contact: '30',
                operator: '40',
                messengerId: '50',
                extraData: [
                    'key' => 'value'
                ]
            ),
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new ChatDataChangedHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }

    public function testInvokeOnMinData(): void
    {
        $input = new ChatDataChangedHandlerInput([
            'chat' => [
                'externalId'        => '10',
                'messengerInstance' => '20',
            ],
        ]);

        $expected = new ChatDataChanged(
            chat: new Chat(
                externalId: '10',
                messengerInstance: '20',
            ),
        );

        $event_handler = new ChatDataChangedHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }
}
