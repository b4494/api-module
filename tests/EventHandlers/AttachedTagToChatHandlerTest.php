<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\EventHandlers;

use BmPlatform\Abstraction\DataTypes\Tag;
use BmPlatform\Abstraction\Events\AttachedTagToChat;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\AttachedTagToChatHandlerInput;
use BmPlatform\ApiModule\EventHandlers\AttachedTagToChatHandler;
use Carbon\Carbon;

class AttachedTagToChatHandlerTest extends AbstractEventHandlerTestCase
{
    public function testInvoke(): void
    {
        $input = new AttachedTagToChatHandlerInput([
            'chat'      => '10',
            'tag'       => '20',
            'forTicket' => true,
            'timestamp' => '1577836800'
        ]);

        $expected = new AttachedTagToChat(
            chat: '10',
            tag: '20',
            forTicket: true,
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new AttachedTagToChatHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }

    public function testInvokeOnFullTagStruct(): void
    {
        $input = new AttachedTagToChatHandlerInput([
            'chat' => '10',
            'tag'  => [
                'externalId' => '1',
                'label'      => 'tag',
                'group'      => 'group'
            ],
            'forTicket' => true,
            'timestamp' => '1577836800'
        ]);

        $expected = new AttachedTagToChat(
            chat: '10',
            tag: new Tag(
                externalId: '1',
                label: 'tag',
                group: 'group'
            ),
            forTicket: true,
            timestamp: Carbon::parse('2020-01-01 00:00:00')
        );

        $event_handler = new AttachedTagToChatHandler();
        $result = $event_handler($input);

        $this->assertEquals($expected, $result);
    }
}
