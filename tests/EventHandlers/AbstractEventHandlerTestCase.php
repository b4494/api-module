<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\EventHandlers;

use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub as HubInterface;
use BmPlatform\ApiModule\Entities\Integration;
use BmPlatform\ApiModule\IntegrationRepository;
use BmPlatform\ApiModule\Tests\TestCase;
use Mockery;

class AbstractEventHandlerTestCase extends TestCase
{
    protected HubInterface|Mockery\MockInterface $hub;
    protected AppInstance|Mockery\MockInterface $app_instance;
    protected IntegrationRepository|Mockery\MockInterface $integration_repository;

    public function setUp(): void
    {
        parent::setUp();

        $this->mockTranslatorIntoDI();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }
}
