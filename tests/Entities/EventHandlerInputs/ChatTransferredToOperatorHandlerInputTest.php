<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Entities\EventHandlerInputs;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ChatTransferredToOperatorHandlerInput;
use BmPlatform\ApiModule\Tests\TestCase;

class ChatTransferredToOperatorHandlerInputTest extends TestCase
{
    /**
     * @dataProvider validationFailuresProvider
     */
    public function testValidationFailures(array $data): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => new ChatTransferredToOperatorHandlerInput($data)
        );
    }

    public function validationFailuresProvider(): array
    {
        return [
            'missing chat external id' => [
                'data' => [
                    'newOperator'  => '20',
                    'prevOperator' => '30',
                    'timestamp'    => '1577836800'
                ]
            ],
            'missing previous operator' => [
                'data' => [
                    'chat'        => '10',
                    'newOperator' => '20',
                    'timestamp'   => '1577836800'
                ]
            ]
        ];
    }
}
