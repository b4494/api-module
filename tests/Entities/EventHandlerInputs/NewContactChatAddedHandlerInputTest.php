<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Entities\EventHandlerInputs;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\NewContactChatAddedHandlerInput;
use BmPlatform\ApiModule\Tests\TestCase;

class NewContactChatAddedHandlerInputTest extends TestCase
{
    /**
     * @dataProvider validationFailuresProvider
     */
    public function testValidationFailures(array $data): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => new NewContactChatAddedHandlerInput($data)
        );
    }

    public function validationFailuresProvider(): array
    {
        return [
            'missing chat messenger instance' => [
                'data' => [
                    'chat' => [
                        'externalId' => '10',
                    ],
                    'contact' => [
                        'externalId' => '10',
                    ],
                    'timestamp' => '1577836800'
                ]
            ],
            'missing contact external id' => [
                'data' => [
                    'chat' => [
                        'externalId'        => '10',
                        'messengerInstance' => '20'
                    ],
                    'timestamp' => '1577836800'
                ]
            ]
        ];
    }
}
