<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Entities\EventHandlerInputs;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ChatTicketClosedHandlerInput;
use BmPlatform\ApiModule\Tests\TestCase;

class ChatTicketClosedHandlerInputTest extends TestCase
{
    /**
     * @dataProvider validationFailuresProvider
     */
    public function testValidationFailures(array $data): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => new ChatTicketClosedHandlerInput($data)
        );
    }

    public function validationFailuresProvider(): array
    {
        return [
            'missing chat external id' => [
                'data' => [
                    'chat'      => '10',
                    'timestamp' => '1577836800'
                ]
            ],
            'missing external ticket id' => [
                'data' => [
                    'ticketId'  => '20',
                    'timestamp' => '1577836800'
                ]
            ]
        ];
    }
}
