<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Entities\EventHandlerInputs;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\OutboxSentHandlerInput;
use BmPlatform\ApiModule\Tests\TestCase;

class OutboxSentHandlerInputTest extends TestCase
{
    /**
     * @dataProvider validationFailuresProvider
     */
    public function testValidationFailures(array $data): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => new OutboxSentHandlerInput($data)
        );
    }

    public function validationFailuresProvider(): array
    {
        return [
            'missing chat external id' => [
                'data' => [
                    'message' => [
                        'externalId' => '30',
                        'text'       => 'hola',
                    ],
                    'operator'  => '40',
                    'timestamp' => '1577836800'
                ]
            ],
            'missing message' => [
                'data' => [
                    'chat'      => '10',
                    'operator'  => '40',
                    'timestamp' => '1577836800'
                ]
            ],
            'incorrect attachments' => [
                'data' => [
                    'chat'    => '10',
                    'message' => [
                        'externalId'  => '30',
                        'text'        => 'hola',
                        'attachments' => [
                            'type' => 'Image',
                            'url'  => 'https://example.com/image.jpg',
                            'mime' => 'image/jpeg'
                        ],
                    ],
                    'operator'  => '40',
                    'timestamp' => '1577836800'
                ]
            ],
        ];
    }
}
