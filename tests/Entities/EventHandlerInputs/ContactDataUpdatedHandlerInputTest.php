<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Entities\EventHandlerInputs;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ContactDataUpdatedHandlerInput;
use BmPlatform\ApiModule\Tests\TestCase;

class ContactDataUpdatedHandlerInputTest extends TestCase
{
    /**
     * @dataProvider validationFailuresProvider
     */
    public function testValidationFailures(array $data): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => new ContactDataUpdatedHandlerInput($data)
        );
    }

    public function validationFailuresProvider(): array
    {
        return [
            'missing contact external id' => [
                'data' => [
                    'contact' => [
                        'name' => 'contact',
                    ],
                    'timestamp' => '1577836800'
                ]
            ],
            'incorrect avatar url' => [
                'data' => [
                    'contact' => [
                        'externalId' => '10',
                        'avatarUrl'  => 'url',
                    ],
                    'timestamp' => '1577836800'
                ]
            ]
        ];
    }
}
