<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Entities\EventHandlerInputs;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\DetachedTagFromChatHandlerInput;
use BmPlatform\ApiModule\Tests\TestCase;

class DetachedTagFromChatHandlerInputTest extends TestCase
{
    /**
     * @dataProvider validationFailuresProvider
     */
    public function testValidationFailures(array $data): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => new DetachedTagFromChatHandlerInput($data)
        );
    }

    public function validationFailuresProvider(): array
    {
        return [
            'missing chat external id' => [
                'data' => [
                    'tag'       => '20',
                    'forTicket' => true,
                    'timestamp' => '1577836800'
                ]
            ],
            'missing label of tag' => [
                'data' => [
                    'chat' => '10',
                    'tag'  => [
                        'externalId' => '10',
                    ],
                    'forTicket' => true,
                    'timestamp' => '1577836800'
                ]
            ],
        ];
    }
}
