<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Entities\EventHandlerInputs;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ChatDataChangedHandlerInput;
use BmPlatform\ApiModule\Tests\TestCase;

class ChatDataChangedHandlerInputTest extends TestCase
{
    /**
     * @dataProvider validationFailuresProvider
     */
    public function testValidationFailures(array $data): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => new ChatDataChangedHandlerInput($data)
        );
    }

    public function validationFailuresProvider(): array
    {
        return [
            'chat missing' => [
                'data' => []
            ],
            'chat external id missing' => [
                'data' => [
                    'externalId' => '10',
                ]
            ]
        ];
    }
}
