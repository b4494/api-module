<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Entities\EventHandlerInputs;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\AttachedTagToChatHandlerInput;
use BmPlatform\ApiModule\Tests\TestCase;

class AttachedTagToChatHandlerInputTest extends TestCase
{
    /**
     * @dataProvider validationFailuresProvider
     */
    public function testValidationFailures(array $data): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => new AttachedTagToChatHandlerInput($data)
        );
    }

    public function validationFailuresProvider(): array
    {
        return [
            'missing chat external id' => [
                'data' => [
                    'tag'       => '20',
                    'forTicket' => true,
                    'timestamp' => '1577836800'
                ]
            ],
            'incorrect for ticket type' => [
                'data' => [
                    'chat'      => '10',
                    'tag'       => '20',
                    'forTicket' => 'yes',
                    'timestamp' => '1577836800'
                ]
            ],
            'missing tag' => [
                'data' => [
                    'chat'      => '10',
                    'forTicket' => true,
                    'timestamp' => '1577836800'
                ]
            ],
            'missing chat information' => [
                'data' => [
                    'chat' => [
                        'externalId' => '1'
                    ],
                    'tag'       => '20',
                    'forTicket' => true,
                    'timestamp' => '1577836800'
                ]
            ]
        ];
    }
}
