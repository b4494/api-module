<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Entities\EventHandlerInputs;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\InboxReceivedHandlerInput;
use BmPlatform\ApiModule\Tests\TestCase;

class InboxReceivedHandlerInputTest extends TestCase
{
    /**
     * @dataProvider validationFailuresProvider
     */
    public function testValidationFailures(array $data): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => new InboxReceivedHandlerInput($data)
        );
    }

    public function validationFailuresProvider(): array
    {
        return [
            'missing chat external id' => [
                'data' => [
                    'participant' => '20',
                    'message'     => [
                        'externalId' => '30',
                        'text'       => 'hola',
                    ],
                    'timestamp' => '1577836800'
                ]
            ],
            'missing participant external id' => [
                'data' => [
                    'chat'    => '10',
                    'message' => [
                        'externalId' => '30',
                        'text'       => 'hola',
                    ],
                    'timestamp' => '1577836800'
                ]
            ],
            'missing message' => [
                'data' => [
                    'chat'        => '10',
                    'participant' => '20',
                    'timestamp'   => '1577836800'
                ]
            ]
        ];
    }
}
