<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Entities\EventHandlerInputs;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\MessageStatusChangedHandlerInput;
use BmPlatform\ApiModule\Tests\TestCase;

class MessageStatusChangedHandlerInputTest extends TestCase
{
    /**
     * @dataProvider validationFailuresProvider
     */
    public function testValidationFailures(array $data): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => new MessageStatusChangedHandlerInput($data)
        );
    }

    public function validationFailuresProvider(): array
    {
        return [
            'incorrect status' => [
                'data' => [
                    'chat'      => '10',
                    'messageId' => '20',
                    'status'    => 'Received Message',
                    'timestamp' => '1577836800'
                ]
            ],
            'missing message id' => [
                'data' => [
                    'chat'      => '10',
                    'status'    => 'Sent',
                    'timestamp' => '1577836800'
                ]
            ]
        ];
    }
}
