<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Exceptions\ErrorException;
use Closure;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Support\Facades\App;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;

class TestCase extends MockeryTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        App::clearResolvedInstances();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    protected function mockTranslatorIntoDI(int $times = 1): Mockery\MockInterface|Translator
    {
        $translator = Mockery::mock(Translator::class);

        App::expects('make')->with(Translator::class)->times($times)->andReturn($translator);

        return $translator;
    }

    protected function expectErrorExceptionWithCode(ErrorCode $expected_code, Closure $closure): void
    {
        try {
            $closure();
        } catch (ErrorException $e) {
            $this->assertEquals($expected_code, $e->errorCode);

            return;
        }

        $this->fail('Missed expected Error Exception');
    }
}
