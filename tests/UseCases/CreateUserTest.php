<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\UseCases;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\DataTypes\AppIntegrationData;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\PaymentStatus;
use BmPlatform\Abstraction\Enums\PaymentType;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\ApiModule\Entities\Integration;
use BmPlatform\ApiModule\IntegrationRepository;
use BmPlatform\ApiModule\Tests\TestCase;
use BmPlatform\ApiModule\UseCases\CreateUser;
use Carbon\CarbonTimeZone;
use Mockery;

class CreateUserTest extends TestCase
{
    public function testExecute(): void
    {
        $this->mockTranslatorIntoDI();

        $app_instance = Mockery::mock(AppInstance::class);

        $hub = Mockery::mock(Hub::class);
        $hub->expects('integrate')->withArgs(function (AppIntegrationData $app_integration_data) {
            $expected = new AppIntegrationData(
                type: 'api1',
                externalId: '10',
                name: 'Partner',
                locale: 'ru',
                timeZone: CarbonTimeZone::createFromHourOffset(7),
                email: 'example@example.com',
                extraData: [
                    'id_1' => '1',
                    'id_2' => '2'
                ],
                paymentType: PaymentType::External,
                status: new AppExternalStatus(AppStatus::Active, PaymentStatus::Paid),
            );

            $this->assertEquals($expected, $app_integration_data);

            return true;
        })->once()->andReturn($app_instance);

        $hub->expects('getAuthToken')->with($app_instance)->once()->andReturn('abc');
        $hub->expects('webUrl')->with('abc')->once()->andReturn('https://auth_url.com/?access_token=abc');

        $integration_repository = Mockery::mock(IntegrationRepository::class);
        $integration_repository->expects('getByCodeOrFail')->with('api1')->andReturn(new Integration(
            code: 'api1',
            url: 'https://example.com',
            key: 'secret'
        ));

        $use_case = new CreateUser($integration_repository, $hub);
        $url = $use_case->execute('api1', [
            'externalId' => '10',
            'name'       => 'Partner',
            'locale'     => 'ru',
            'timeZone'   => '+07:00',
            'email'      => 'example@example.com',
            'paymentType' => 'external',
            'status' => [
                'status' => 'active',
                'paymentStatus' => 'paid',
            ],
            'extraData'  => [
                'id_1' => '1',
                'id_2' => '2'
            ]
        ]);

        $this->assertEquals(
            'https://auth_url.com/?access_token=abc',
            $url
        );
    }

    /**
     * @dataProvider validationFailuresProvider
     */
    public function testValidationFailures(array $data): void
    {
        $translator = $this->mockTranslatorIntoDI();
        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');

        $integration_repository = Mockery::mock(IntegrationRepository::class);
        $integration_repository->expects('getByCodeOrFail')->with('api1')->andReturn(new Integration(
            code: 'api1',
            url: 'https://example.com',
            key: 'secret'
        ));

        $use_case = new CreateUser($integration_repository, Mockery::mock(Hub::class));

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => $use_case->execute('api1', $data)
        );
    }

    public function validationFailuresProvider(): array
    {
        return [
            'missing external id' => [
                'data' => [
                    'name'      => 'Partner',
                    'locale'    => 'ru',
                    'timeZone'  => '+07:00',
                    'email'     => 'example@example.com',
                    'extraData' => [
                        'id_1' => '1',
                        'id_2' => '2'
                    ]
                ]
            ],
//            'incorrect timezone' => [
//                'data' => [
//                    'externalId' => '10',
//                    'name'       => 'Partner',
//                    'locale'     => 'ru',
//                    'timeZone'   => 'plus seven',
//                    'email'      => 'example@example.com',
//                    'extraData'  => [
//                        'id_1' => '1',
//                        'id_2' => '2'
//                    ]
//                ],
//            ],
            'incorrect email' => [
                'data' => [
                    'externalId' => '10',
                    'name'       => 'Partner',
                    'locale'     => 'ru',
                    'timeZone'   => '+07:00',
                    'email'      => 'example[at]]example.com',
                    'extraData'  => [
                        'id_1' => '1',
                        'id_2' => '2'
                    ]
                ],
            ],
        ];
    }
}
