<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\UseCases;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub as HubInterface;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ChatTicketClosedHandlerInput;
use BmPlatform\ApiModule\Entities\Integration;
use BmPlatform\ApiModule\EventHandlers\AbstractHandler;
use BmPlatform\ApiModule\EventHandlers\ChatTicketClosedHandler;
use BmPlatform\ApiModule\Exceptions\ErrorException;
use BmPlatform\ApiModule\IntegrationRepository;
use BmPlatform\ApiModule\Tests\TestCase;
use BmPlatform\ApiModule\UseCases\ProcessEvent;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event as EventFacade;
use Mockery;

class ProcessEventTest extends TestCase
{
    private HubInterface|Mockery\MockInterface $hub;
    private IntegrationRepository|Mockery\MockInterface $integration_repository;

    /**
     * @return void
     * @throws ReflectionException
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->hub = Mockery::mock(HubInterface::class);
        $this->integration_repository = Mockery::mock(IntegrationRepository::class);

        $this->integration_repository->expects('getByCodeOrFail')->with('api1')->andReturn(new Integration(
            code: 'api1',
            url: 'https://example.com',
            key: 'secret'
        ))->zeroOrMoreTimes();
    }

    public function tearDown(): void
    {
        parent::tearDown();

        unset($this->hub, $this->integration_repository);
    }

    public function testExecute(): void
    {
        $this->mockTranslatorIntoDI(2);

        $data = [
            'eventType' => 'ChatTicketClosed',
            'ticketId'  => '20',
            'chat'      => '10',
            'timestamp' => '1577836800'
        ];

        $handler = Mockery::mock(ChatTicketClosedHandler::class);
        $handler->expects('__invoke')->withArgs(function (ChatTicketClosedHandlerInput $input) use ($data) {
            $this->assertInstanceOf(ChatTicketClosedHandlerInput::class, $input);

            return true;
        });

        App::expects('make')
            ->with(ChatTicketClosedHandler::class)
            ->once()
            ->andReturn($handler);

        $appInstance = Mockery::mock(AppInstance::class);
        $appInstance->expects('dispatchEvent');

        $use_case = new ProcessEvent($this->integration_repository, $this->hub);

        $this->hub->expects('findAppById')->with('api1', '1')->andReturn($appInstance);

        $use_case->execute('api1', '1', $data);
    }

//    public function testExecuteOnUnknownEvent(): void
//    {
//        $translator = $this->mockTranslatorIntoDI();
//        $translator->expects('get')->zeroOrMoreTimes()->andReturn('Incorrect!');
//
//        $data = [
//            'eventType' => 'UNKNOWN_TYPE'
//        ];
//
//        $use_case = new ProcessEvent($this->integration_repository, $this->hub);
//
//        $this->expectErrorExceptionWithCode(ErrorCode::DataMissing, fn () => $use_case->execute('api1', '1', $data));
//    }
//
//
//
//    /**
//     * @return void
//     * @throws ReflectionException
//     */
//    public function testFindAppInstanceOrFail(): void
//    {
//        $app_instance = Mockery::mock(AppInstance::class);
//
//        $this->hub->expects('findAppById')
//            ->with('api1', '1')
//            ->once()
//            ->andReturn($app_instance);
//
//        $reflector_method = $this->reflector->getMethod('findAppInstanceOrFail');
//
//        $this->assertEquals(
//            $app_instance,
//            $reflector_method->invokeArgs($this->abstract_handler, ['api1', '1'])
//        );
//    }
//
    /**
     * @return void
     * @throws ReflectionException
     */
    public function testFindAppInstanceOrFailOnHubFail(): void
    {
        $this->hub->expects('findAppById')
            ->with('api1', '1')
            ->once()
            ->andReturn(null);

        $useCase = new ProcessEvent($this->integration_repository, $this->hub);

        $this->expectErrorExceptionWithCode(
            ErrorCode::AuthenticationFailed,
            fn () => $useCase->findAppInstanceOrFail('api1', '1'),
        );
    }

    /**
     * @return void
     * @throws ReflectionException
     */
    public function testFindAppInstanceOrFailOnIntegrationRepositoryFail(): void
    {
        $this->integration_repository->expects('getByCodeOrFail')->with('api2')->andThrow(new ErrorException(ErrorCode::DataMissing, 'unknown code'));

        $useCase = new ProcessEvent($this->integration_repository, $this->hub);

        $this->expectErrorExceptionWithCode(
            ErrorCode::DataMissing,
            fn () => $useCase->findAppInstanceOrFail('api2', '1'),
        );
    }
}
