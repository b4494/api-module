<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\ApiClient;
use Closure;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Mockery;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;

class ApiClientTest extends TestCase
{
    private Mockery\MockInterface|CacheInterface $cache;
    private LoggerInterface $logger;

    public function setUp(): void
    {
        parent::setUp();

        $this->cache = Mockery::mock(CacheInterface::class);
        $this->logger = Mockery::mock(LoggerInterface::class);
    }

    public function tearDown(): void
    {
        parent::tearDown();

        unset($this->cache, $this->logger);
    }

    public function testGet(): void
    {
        $this->logger->expects('info')->twice();

        $response = $this->makeApiClient(function (Request $request) {
            $this->assertEquals('GET', $request->getMethod());
            $this->assertEquals('https://example.com/resource?never_gonna=let_you_down', (string) $request->getUri());

            return new Response(200, [], '{"result":{"never_gonna_say":"good_bye"}}');
        })->get('/resource', ['never_gonna' => 'let_you_down']);

        $this->assertEquals([
            'result' => [
                'never_gonna_say' => 'good_bye'
            ]
        ], $response);
    }

    public function testPost(): void
    {
        $this->logger->expects('info')->twice();

        $response = $this->makeApiClient(function (Request $request) {
            $this->assertEquals('POST', $request->getMethod());
            $this->assertEquals('https://example.com/resource', (string) $request->getUri());
            $this->assertEquals('{"never_gonna":"let_you_down"}', (string) $request->getBody());

            return new Response(200, [], '{"result":{"never_gonna_say":"good_bye"}}');
        })->post('/resource', ['never_gonna' => 'let_you_down']);

        $this->assertEquals([
            'result' => [
                'never_gonna_say' => 'good_bye'
            ]
        ], $response);
    }

    public function testGetCachedOnEmptyCache(): void
    {
        $this->logger->expects('info')->twice();

        $this->cache->expects('get')->with('api_module_GET/resource_never_gonna=let_you_down')->once()->andReturn(null);
        $this->cache->expects('set')->with(
            'api_module_GET/resource_never_gonna=let_you_down',
            ['result' => ['never_gonna_say' => 'good_bye']],
            86400
        )->once()->andReturn(true);

        $api_client =  $this->makeApiClient(function (Request $request) {
            $this->assertEquals('GET', $request->getMethod());
            $this->assertEquals('https://example.com/resource?never_gonna=let_you_down', (string) $request->getUri());

            return new Response(200, [], '{"result":{"never_gonna_say":"good_bye"}}');
        });

        $this->assertEquals([
            'result' => [
                'never_gonna_say' => 'good_bye'
            ]
        ], $api_client->getCached('/resource', ['never_gonna' => 'let_you_down']));

        $this->assertEquals([
            'result' => [
                'never_gonna_say' => 'good_bye'
            ]
        ], $api_client->getCached('/resource', ['never_gonna' => 'let_you_down']));
    }

    public function testGetCachedOnCached(): void
    {
        $this->cache->expects('get')->with('api_module_GET/resource_never_gonna=let_you_down')->once()->andReturn(['result' => ['never_gonna_say' => 'good_bye']]);

        $api_client =  $this->makeApiClient();

        $this->assertEquals([
            'result' => [
                'never_gonna_say' => 'good_bye'
            ]
        ], $api_client->getCached('/resource', ['never_gonna' => 'let_you_down']));
    }

    public function testSendRestApiRequestOnSuccess(): void
    {
        $this->logger->expects('info')->twice();

        $response = $this->makeApiClient(function (Request $request) {
            $this->assertEquals('GET', $request->getMethod());
            $this->assertEquals('https://example.com?never_gonna=let_you_down', (string) $request->getUri());

            return new Response(200, [], '{"result":{"never_gonna_say":"good_bye"}}');
        })->request('GET', 'https://example.com', ['query' => ['never_gonna' => 'let_you_down']]);

        $this->assertEquals([
            'result' => [
                'never_gonna_say' => 'good_bye'
            ]
        ], $response);
    }

    /**
     * @dataProvider sendRestApiRequestOnErrorProvider
     */
    public function testProcessResponseOnError(ErrorCode $expected_code, int $response_code, string $body): void
    {
        $this->logger->expects('info')->twice();

        $this->expectErrorExceptionWithCode($expected_code, function () use ($response_code, $body) {
            $this->makeApiClient(fn () => new Response($response_code, [], $body))
                ->request('GET', 'https://example.com', ['query' => ['never_gonna' => ' let_you_down']]);
        });
    }

    public function sendRestApiRequestOnErrorProvider(): array
    {
        return [
            'on internal_server_error code' => [ErrorCode::InternalServerError, 500, '{"code":"internal_server_error","message":"Internal Server Error"}'],
            'on not_found code'             => [ErrorCode::NotFound, 404, '{"code":"not_found","message":"Not Found"}'],
            'on unexpected code'            => [ErrorCode::UnexpectedServerError, 400, '{"code":nothing_here,"message":"Not Found"}'],
            'not supported'            => [ErrorCode::FeatureNotSupported, 404, ''],
        ];
    }

    private function makeApiClient(Closure ...$response_closure): ApiClient
    {
        $handler = new MockHandler($response_closure);

        return new ApiClient(
            base_url: 'https://example.com',
            secret_key: 'super_secret',
            cache: $this->cache,
            logger: $this->logger,
            options: ['handler' => $handler]
        );
    }
}
