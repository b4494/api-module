<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule {

    function file_exists(string $path)
    {
        return true;
    }

    function file_get_contents(string $path, bool $isAssociative = true)
    {
        return '[{"code":"10","schema_data":{"key":"value"},"updated_at":"2020-01-01 00:00:00"},{"code":"20","schema_data":{"key2":"value2"},"updated_at":"2020-01-02 00:00:00"}]';
    }

    function storage_path(string $path)
    {
        return $path;
    }

    function file_put_contents($path, $data)
    {
        return 1;
    }
}

namespace BmPlatform\ApiModule\Tests {

    use BmPlatform\ApiModule\Entities\Schema;
    use BmPlatform\ApiModule\IntegrationSchemaRepository;
    use Carbon\Carbon;

    class IntegrationSchemaRepositoryTest extends TestCase
    {
        private IntegrationSchemaRepository $integration_schema_repository;

        public function setUp(): void
        {
            parent::setUp();

            $this->integration_schema_repository = new IntegrationSchemaRepository();
        }

        public function tearDown(): void
        {
            parent::tearDown();

            unset($this->integration_schema_repository);
        }

        public function testGetAll(): void
        {
            $this->assertEquals([
                '10' => new Schema('10', ['key' => 'value'], Carbon::parse('2020-01-01 00:00:00')),
                '20' => new Schema('20', ['key2' => 'value2'], Carbon::parse('2020-01-02 00:00:00'))
            ], $this->integration_schema_repository->getAll());
        }

        public function testGetByCode(): void
        {
            $this->assertEquals(
                new Schema('10', ['key' => 'value'], Carbon::parse('2020-01-01 00:00:00')),
                $this->integration_schema_repository->getByCode('10')
            );
            $this->assertNull($this->integration_schema_repository->getByCode('40'));
        }

        public function testUpdateSchemas(): void
        {
            $this->integration_schema_repository->updateSchemas([
                new Schema('10', ['key' => 'value'], Carbon::parse('2020-01-01 00:00:00')),
                new Schema('20', ['key2' => 'value2'], Carbon::parse('2020-01-02 00:00:00'))
            ]);
        }
    }
}
