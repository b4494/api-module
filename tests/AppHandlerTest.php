<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests;

use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Abstraction\DataTypes\OperatorGroup;
use BmPlatform\Abstraction\DataTypes\Tag;
use BmPlatform\Abstraction\Enums\MessengerType as InternalMessengerTypeEnum;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\ApiModule\ApiIntegrationUserCommands;
use BmPlatform\ApiModule\AppHandler;
use BmPlatform\ApiModule\Commands;
use BmPlatform\ApiModule\Entities\Integration;
use Illuminate\Support\Facades\App;
use Mockery;

class AppHandlerTest extends TestCase
{
    private AppHandler $app_handler;
    private ApiIntegrationUserCommands|Mockery\MockInterface $api_commands;
    private AppInstance|Mockery\MockInterface $app_instance;

    public function setUp(): void
    {
        parent::setUp();

        $this->app_instance = Mockery::mock(AppInstance::class);
        $this->app_instance->expects('getExternalId')->zeroOrMoreTimes()->andReturn('1');

        $this->api_commands = Mockery::mock(ApiIntegrationUserCommands::class);

        $integration = new Integration(
            code: 'api',
            url: 'https://example.com/api',
            key: 'something_secret'
        );

        App::expects('make')
            ->with(ApiIntegrationUserCommands::class, [
                'integration'     => $integration,
                'app_external_id' => '1'
            ])
            ->zeroOrMoreTimes()
            ->andReturn($this->api_commands);

        $this->app_handler = new AppHandler(
            $this->app_instance,
            $integration
        );
    }

    public function tearDown(): void
    {
        parent::tearDown();

        unset($this->app_handler, $this->api_commands, $this->app_instance);
    }

    public function testGetMessengerInstances(): void
    {
        $expected = new IterableData([
            new MessengerInstance(
                externalType: 'whatsappbytwilio',
                externalId: '1|whatsappbytwilio',
                name: 'WhatsApp by Twilio',
                description: 'WhatsApp by Twilio',
                messengerId: null,
                internalType: InternalMessengerTypeEnum::Whatsapp,
            ),
        ], null);

        $this->api_commands->expects('getMessengers')->once()->andReturn($expected);

        $this->assertEquals($expected, $this->app_handler->getMessengerInstances());
    }

    public function testGetOperators(): void
    {
        $expected = new IterableData([
            new Operator(
                externalId: '1',
                name: 'name',
                phone: '+79998884433',
                email: 'example@example.com'
            )
        ], null);

        $this->api_commands->expects('getOperators')->once()->andReturn($expected);

        $this->assertEquals($expected, $this->app_handler->getOperators());
    }

    public function testGetOperatorGroups(): void
    {
        $expected = new IterableData([
            new OperatorGroup(
                externalId: '1',
                name: 'name',
            )
        ], null);

        $this->api_commands->expects('getOperatorGroups')->once()->andReturn($expected);

        $this->assertEquals($expected, $this->app_handler->getOperatorGroups());
    }

    public function testGetTags(): void
    {
        $expected = new IterableData([
            new Tag(
                externalId: '1',
                label: 'label',
            )
        ], null);

        $this->api_commands->expects('getTags')->once()->andReturn($expected);

        $this->assertEquals($expected, $this->app_handler->getTags());
    }

    public function testGetCommands(): void
    {
        App::expects('make')
            ->with(Commands::class, [
                'api_commands' => $this->api_commands
            ])
            ->once()
            ->andReturn(Mockery::mock(Commands::class));

        $this->assertInstanceOf(Commands::class, $this->app_handler->getCommands());
    }
}
