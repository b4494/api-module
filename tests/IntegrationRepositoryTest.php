<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Entities\Integration;
use BmPlatform\ApiModule\IntegrationRepository;
use BmPlatform\ApiModule\ServiceProvider;
use Illuminate\Config\Repository;
use Mockery;

class IntegrationRepositoryTest extends TestCase
{
    private Repository|Mockery\MockInterface $config;
    private IntegrationRepository $integration_repository;

    public function setUp(): void
    {
        parent::setUp();

        $this->config = Mockery::mock(Repository::class);
        $this->config->expects('get')->with(ServiceProvider::CONFIG_KEY . '.integrations', [])->once()->andReturn([
            [
                'code' => 'api1',
                'url'  => 'https://example.com/api',
                'key'  => 'super_secret'
            ],
            [
                'code' => 'api2',
                'url'  => 'https://example.com/api',
                'key'  => 'super_secret'
            ]
        ]);

        $this->integration_repository = new IntegrationRepository($this->config);
    }

    public function tearDown(): void
    {
        parent::tearDown();

        unset($this->config, $this->integration_repository);
    }

    public function testGetAll(): void
    {
        $this->assertEquals([
            'api1' => new Integration(
                code: 'api1',
                url: 'https://example.com/api',
                key: 'super_secret'
            ),
            'api2' => new Integration(
                code: 'api2',
                url: 'https://example.com/api',
                key: 'super_secret'
            ),
        ], $this->integration_repository->getAll());
    }

    public function testGetByCode(): void
    {
        $this->assertEquals(
            new Integration(
                code: 'api2',
                url: 'https://example.com/api',
                key: 'super_secret'
            ),
            $this->integration_repository->getByCodeOrFail('api2')
        );
    }

    public function testGetByCodeOnFail(): void
    {
        $this->expectErrorExceptionWithCode(
            ErrorCode::NotFound,
            fn () => $this->integration_repository->getByCodeOrFail('api3')
        );
    }
}
