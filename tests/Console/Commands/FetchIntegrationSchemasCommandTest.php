<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Tests\Console\Commands;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\ApiIntegrationCommands;
use BmPlatform\ApiModule\Console\Commands\FetchIntegrationSchemasCommand;
use BmPlatform\ApiModule\Entities\Integration;
use BmPlatform\ApiModule\Entities\Schema;
use BmPlatform\ApiModule\Exceptions\ErrorException;
use BmPlatform\ApiModule\IntegrationRepository;
use BmPlatform\ApiModule\IntegrationSchemaRepository;
use BmPlatform\ApiModule\Tests\TestCase;
use BmPlatform\ApiModule\UseCases\FetchIntegrationSchemas;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Mockery;

class FetchIntegrationSchemasCommandTest extends TestCase
{
    public function testHandle(): void
    {
        $integrations = [
            new Integration('10', 'https://example.com/api1', 'key1'),
            new Integration('20', 'https://example.com/api2', 'key2'),
            new Integration('30', 'https://example.com/api3', 'key3')
        ];

        $integration_repository = Mockery::mock(IntegrationRepository::class);
        $integration_repository->expects('getAll')
            ->once()
            ->andReturn($integrations);

        $api_commands_1 = Mockery::mock(ApiIntegrationCommands::class);
        $api_commands_1->expects('getSchema')->once()->andReturn(new Schema('10', ['data'], Carbon::parse('2020-01-01 00:00:00')));

        $api_commands_2 = Mockery::mock(ApiIntegrationCommands::class);
        $api_commands_2->expects('getSchema')->once()->andThrow(new ErrorException(ErrorCode::InvalidResponseData));

        $api_commands_3 = Mockery::mock(ApiIntegrationCommands::class);
        $api_commands_3->expects('getSchema')->once()->andThrow(new ErrorException(ErrorCode::InvalidResponseData));

        App::expects('makeWith')
            ->with(ApiIntegrationCommands::class, [
                'integration' => $integrations[0]
            ])
            ->once()
            ->andReturn($api_commands_1);

        App::expects('makeWith')
            ->with(ApiIntegrationCommands::class, [
                'integration' => $integrations[1]
            ])
            ->once()
            ->andReturn($api_commands_2);

        App::expects('makeWith')
            ->with(ApiIntegrationCommands::class, [
                'integration' => $integrations[2]
            ])
            ->once()
            ->andReturn($api_commands_3);

        $integration_schema_repository = Mockery::mock(IntegrationSchemaRepository::class);
        $integration_schema_repository->expects('getByCode')->with('20')->andReturn(new Schema('20', ['data'], Carbon::parse('2020-01-01 00:00:00')));
        $integration_schema_repository->expects('getByCode')->with('30')->andReturnNull();

        $integration_schema_repository->expects('updateSchemas')->with([
            new Schema('10', ['data'], Carbon::parse('2020-01-01 00:00:00')),
            new Schema('20', ['data'], Carbon::parse('2020-01-01 00:00:00'))
        ])->once();

        $command = new FetchIntegrationSchemasCommand();
        $command->handle(
            new FetchIntegrationSchemas(
                $integration_repository,
                $integration_schema_repository
            )
        );
    }
}
