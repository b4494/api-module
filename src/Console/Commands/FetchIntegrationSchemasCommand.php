<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Console\Commands;

use BmPlatform\ApiModule\UseCases\FetchIntegrationSchemas;
use Illuminate\Console\Command;

class FetchIntegrationSchemasCommand extends Command
{
    protected $name = 'api-module:schemas';
    protected $description = 'Receive and cache all integrations schemas';

    public function handle(FetchIntegrationSchemas $fetch_integration_schemas): void
    {
        $fetch_integration_schemas->execute();
    }
}
