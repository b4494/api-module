<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Http;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\App;

/** @var Router $router */
$router = App::make('router');

$router->namespace('BmPlatform\ApiModule\Http')
    ->name("api-module.")
    ->middleware(BearerTokenMiddleware::class)
    ->prefix("integrations/api/{code}")
    ->group(function () use ($router) {
        $router->post('', [IntegrationController::class, 'createUser'])->name('users.create');
        $router->prefix('/{id}')
            ->group(function () use ($router) {
                $router->delete('', [IntegrationController::class, 'deactivateUser'])->name('users.destroy');
                $router->post('event', [IntegrationController::class, 'processEvent'])->name('events.store');
            });
    });
