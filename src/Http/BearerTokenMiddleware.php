<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Http;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Exceptions\ErrorException;
use BmPlatform\ApiModule\IntegrationRepository;
use Closure;
use Illuminate\Http\Request;

class BearerTokenMiddleware
{
    public function __construct(
        private readonly IntegrationRepository $integration_repository
    ) {
    }

    public function handle(Request $request, Closure $next): mixed
    {
        $integration = $this->integration_repository->getByCodeOrFail((string) $request->route('code'));

        if ($request->headers->get('Authorization') !== 'Bearer ' . $integration->key) {
            throw new ErrorException(ErrorCode::AuthenticationFailed, 'Access Token missed or incorrect');
        }

        return $next($request);
    }
}
