<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Http;

use BmPlatform\ApiModule\Exceptions\ErrorException;
use BmPlatform\ApiModule\UseCases\CreateUser;
use BmPlatform\ApiModule\UseCases\DeactivateUser;
use BmPlatform\ApiModule\UseCases\ProcessEvent;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class IntegrationController
{
    public function createUser(string $integration_code, Request $request, CreateUser $use_case): JsonResponse
    {
        $code = 200;

        try {
            $response = [
                'webUrl' => $use_case->execute($integration_code, $request->input())
            ];
        } catch (ErrorException $e) {
            $response = [
                'message' => $e->getMessage(),
                'code'    => $e->errorCode->name
            ];

            $code = 400;
        }

        return response()->json($response, $code);
    }

    public function deactivateUser(string $integration_code, string $app_external_id, DeactivateUser $use_case): JsonResponse
    {
        $response = null;
        $code = 202;

        try {
            $use_case->execute($integration_code, $app_external_id);
        } catch (ErrorException $e) {
            $response = [
                'message' => $e->getMessage(),
                'code'    => $e->errorCode->name
            ];

            $code = 400;
        }

        return response()->json($response, $code);
    }

    public function processEvent(string $integration_code, string $app_external_id, Request $request, ProcessEvent $use_case): JsonResponse
    {
        $response = null;
        $code = 202;

        try {
            $use_case->execute($integration_code, $app_external_id, $request->input());
        } catch (ErrorException $e) {
            $response = [
                'message' => $e->getMessage(),
                'code'    => $e->errorCode->name
            ];

            $code = 400;
        }

        return response()->json($response, $code);
    }
}
