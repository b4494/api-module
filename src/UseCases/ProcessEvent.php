<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\UseCases;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub as HubInterface;
use BmPlatform\ApiModule\Components\ArrayValidator;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\AttachedTagToChatHandlerInput;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ChatDataChangedHandlerInput;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ChatTicketClosedHandlerInput;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ChatTransferredToOperatorHandlerInput;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ContactDataUpdatedHandlerInput;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\DetachedTagFromChatHandlerInput;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\InboxReceivedHandlerInput;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\MessageStatusChangedHandlerInput;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\NewContactChatAddedHandlerInput;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\OutboxSentHandlerInput;
use BmPlatform\ApiModule\EventHandlers\AttachedTagToChatHandler;
use BmPlatform\ApiModule\EventHandlers\ChatDataChangedHandler;
use BmPlatform\ApiModule\EventHandlers\ChatTicketClosedHandler;
use BmPlatform\ApiModule\EventHandlers\ChatTransferredToOperatorHandler;
use BmPlatform\ApiModule\EventHandlers\ContactDataUpdatedHandler;
use BmPlatform\ApiModule\EventHandlers\DetachedTagFromChatHandler;
use BmPlatform\ApiModule\EventHandlers\InboxReceivedHandler;
use BmPlatform\ApiModule\EventHandlers\MessageStatusChangedHandler;
use BmPlatform\ApiModule\EventHandlers\NewContactChatAddedHandler;
use BmPlatform\ApiModule\EventHandlers\OutboxSentHandler;
use BmPlatform\ApiModule\Exceptions\ErrorException;
use BmPlatform\ApiModule\IntegrationRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event as EventFacade;
use Illuminate\Validation\Rule;

class ProcessEvent
{
    private const EVENT_HANDLER_MAP = [
        'AttachedTagToChat' => [
            'handler' => AttachedTagToChatHandler::class,
            'input'   => AttachedTagToChatHandlerInput::class
        ],
        'ChatDataChanged' => [
            'handler' => ChatDataChangedHandler::class,
            'input'   => ChatDataChangedHandlerInput::class
        ],
        'ChatTicketClosed' => [
            'handler' => ChatTicketClosedHandler::class,
            'input'   => ChatTicketClosedHandlerInput::class
        ],
        'ChatTransferredToOperator' => [
            'handler' => ChatTransferredToOperatorHandler::class,
            'input'   => ChatTransferredToOperatorHandlerInput::class
        ],
        'ContactDataUpdated' => [
            'handler' => ContactDataUpdatedHandler::class,
            'input'   => ContactDataUpdatedHandlerInput::class
        ],
        'DetachedTagFromChat' => [
            'handler' => DetachedTagFromChatHandler::class,
            'input'   => DetachedTagFromChatHandlerInput::class
        ],
        'InboxReceived' => [
            'handler' => InboxReceivedHandler::class,
            'input'   => InboxReceivedHandlerInput::class
        ],
        'MessageStatusChanged' => [
            'handler' => MessageStatusChangedHandler::class,
            'input'   => MessageStatusChangedHandlerInput::class
        ],
        'NewContactChatAdded' => [
            'handler' => NewContactChatAddedHandler::class,
            'input'   => NewContactChatAddedHandlerInput::class
        ],
        'OutboxSent' => [
            'handler' => OutboxSentHandler::class,
            'input'   => OutboxSentHandlerInput::class
        ]
    ];

    public function __construct(
        private readonly IntegrationRepository $integration_repository,
        private readonly HubInterface $hub
    ) {
        //
    }

    public function execute(string $integration_code, string $app_external_id, array $data): void
    {
        $rules = [
            'eventType' => [ 'required', Rule::in(array_keys(self::EVENT_HANDLER_MAP)) ],
        ];

        ArrayValidator::validate($data, $rules, 'Request');

        [ 'handler' => $handler, 'input' => $input ] = self::EVENT_HANDLER_MAP[Arr::get($data, 'eventType')];

        $this->findAppInstanceOrFail($integration_code, $app_external_id)->dispatchEvent(
            App::make($handler)(new $input($data))
        );
    }

    public function findAppInstanceOrFail(string $integration_code, string $id): AppInstance
    {
        $integration = $this->integration_repository->getByCodeOrFail($integration_code);

        $app_instance = $this->hub->findAppById($integration->code, $id);

        if ($app_instance === null) {
            throw new ErrorException(ErrorCode::AuthenticationFailed, 'authentication failed');
        }

        return $app_instance;
    }
}
