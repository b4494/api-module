<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\UseCases;

use BmPlatform\ApiModule\ApiIntegrationCommands;
use BmPlatform\ApiModule\Exceptions\ErrorException;
use BmPlatform\ApiModule\IntegrationRepository;
use BmPlatform\ApiModule\IntegrationSchemaRepository;
use Illuminate\Support\Facades\App;

class FetchIntegrationSchemas
{
    public function __construct(
        private readonly IntegrationRepository $integration_repository,
        private readonly IntegrationSchemaRepository $integration_schema_repository
    ) {
    }

    public function execute(): void
    {
        $schemas = [];

        foreach ($this->integration_repository->getAll() as $integration) {
            dump("Fetching Integration Schema for code $integration->code");

            $api_integration_commands = App::makeWith(ApiIntegrationCommands::class, [
                'integration' => $integration
            ]);

            try {
                $schemas[$integration->code] = $api_integration_commands->getSchema();

                dump("Fetching Integration Schema for code $integration->code - Success");
            } catch (ErrorException $e) {
                dump("Fetching Integration Schema for code $integration->code - Failed: {$e->getMessage()}");

                $cached_schema = $this->integration_schema_repository->getByCode($integration->code);

                if ($cached_schema !== null) {
                    $schemas[$integration->code] = $cached_schema;

                    dump("Using cached integration schema for code $integration->code");

                    continue;
                }

                dump("No cached schema for code $integration->code. Ignoring");
            }
        }

        $this->integration_schema_repository->updateSchemas(array_values($schemas));
    }
}
