<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\UseCases;

use BmPlatform\Abstraction\DataTypes\AppIntegrationData;
use BmPlatform\Abstraction\Enums\PaymentType;
use BmPlatform\Abstraction\Interfaces\Hub as HubInterface;
use BmPlatform\ApiModule\Components\ArrayValidator;
use BmPlatform\ApiModule\IntegrationRepository;
use BmPlatform\ApiModule\Presenters\AppExternalStatusPresenter;
use Carbon\CarbonTimeZone;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class CreateUser
{
    public function __construct(
        private readonly IntegrationRepository $integration_repository,
        private readonly HubInterface $hub
    ) {
    }

    public function execute(string $integration_code, array $data): string
    {
        $integration = $this->integration_repository->getByCodeOrFail($integration_code);

        $rules = [
            'externalId' => ['required', 'string'],
            'name'       => ['required', 'string'],
            'locale'     => ['required', 'string'],
            'timeZone'   => ['filled'],
            'email'      => ['filled', 'email'],
            'paymentType' => [ 'filled', Rule::in(Arr::pluck(PaymentType::cases(), 'value')) ],
            ...(AppExternalStatusPresenter::rules('status.')),
            'extraData'  => ['nullable', 'array']
        ];

        ArrayValidator::validate($data, $rules, 'Request');

        $app_instance = $this->hub->integrate(
            new AppIntegrationData(
                type: $integration->code,
                externalId: $data['externalId'],
                name: $data['name'],
                locale: $data['locale'],
                timeZone: isset($data['timeZone']) ? new CarbonTimeZone($data['timeZone']) : null,
                email: $data['email'] ?? null,
                extraData: $data['extraData'] ?? null,
                paymentType: PaymentType::tryFrom($data['paymentType'] ?? 'internal'),
                status: isset($data['status']) ? AppExternalStatusPresenter::makeStruct($data['status']) : null,
            )
        );

        return $this->hub->webUrl($this->hub->getAuthToken($app_instance));
    }
}
