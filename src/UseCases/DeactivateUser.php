<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\UseCases;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\DataTypes\AppIntegrationData;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Interfaces\Hub as HubInterface;
use BmPlatform\ApiModule\Exceptions\ErrorException;

class DeactivateUser
{
    public function __construct(private readonly HubInterface $hub)
    {
        //
    }

    public function execute(string $integration_code, string $app_instance_id): void
    {
        if (!$app = $this->hub->findAppById($integration_code, $app_instance_id)) {
            throw new ErrorException(ErrorCode::NotFound);
        }

        $app->updateStatus(new AppExternalStatus(AppStatus::Disabled));
    }
}
