<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule;

use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Interfaces\Commands\Commands as CommandsInterface;
use BmPlatform\Abstraction\Interfaces\Commands\SendsSystemMessages;
use BmPlatform\Abstraction\Interfaces\Commands\SupportsChatTags;
use BmPlatform\Abstraction\Interfaces\Commands\SupportsChatTickets;
use BmPlatform\Abstraction\Interfaces\Commands\TransfersChatToOperator;
use BmPlatform\Abstraction\Interfaces\Commands\UpdatesContactData;
use BmPlatform\Abstraction\Requests\ChatTagRequest;
use BmPlatform\Abstraction\Requests\SendMediaRequest;
use BmPlatform\Abstraction\Requests\SendSystemMessageRequest;
use BmPlatform\Abstraction\Requests\SendTextMessageRequest;
use BmPlatform\Abstraction\Requests\TransferChatToOperatorRequest;
use BmPlatform\Abstraction\Requests\UpdateContactDataRequest;
use BmPlatform\Abstraction\Responses\MessageSendResult;
use BmPlatform\Abstraction\Responses\NewOperatorResponse;

class Commands implements CommandsInterface, SendsSystemMessages, SupportsChatTags, SupportsChatTickets, TransfersChatToOperator, UpdatesContactData
{
    public function __construct(
        private readonly ApiIntegrationUserCommands $api_commands
    ) {
    }

    public function sendTextMessage(SendTextMessageRequest $request): MessageSendResult
    {
        return $this->api_commands->sendTextMessage($request);
    }

    public function sendMediaMessage(SendMediaRequest $request): MessageSendResult
    {
        return $this->api_commands->sendMediaMessage($request);
    }

    public function sendSystemMessage(SendSystemMessageRequest $request): void
    {
        $this->api_commands->sendSystemMessage($request);
    }

    public function attachTagToChat(ChatTagRequest $request): void
    {
        $this->api_commands->attachTagToChat($request);
    }

    public function detachTagFromChat(ChatTagRequest $request): void
    {
        $this->api_commands->detachTagFromChat($request);
    }

    public function closeChatTicket(Chat $chat): void
    {
        $this->api_commands->closeChatTicket($chat);
    }

    public function transferChatToOperator(TransferChatToOperatorRequest $request): NewOperatorResponse
    {
        return $this->api_commands->transferChatToOperator($request);
    }

    public function updateContactData(UpdateContactDataRequest $request): void
    {
        $this->api_commands->updateContactData($request);
    }
}
