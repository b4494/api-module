<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Components\ArrayValidator;
use BmPlatform\ApiModule\Entities\Schema;
use BmPlatform\ApiModule\Presenters\VariablePresenter;
use BmPlatform\Support\SchemaValidator;
use BmPlatform\ApiModule\Entities\Integration;
use BmPlatform\ApiModule\Exceptions\ErrorException;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use InvalidArgumentException;

class ApiIntegrationCommands
{
    private readonly ApiClient $client;

    public function __construct(
        private readonly Integration $integration
    ) {
        $this->client = App::makeWith(ApiClient::class, [
            'base_url'   => $integration->url,
            'secret_key' => $integration->key
        ]);
    }

    public function getSchema(): Schema
    {
        $response = $this->client->get('/schema');

        $validator = new SchemaValidator();

        try {
            $validator($response);
        } catch (InvalidArgumentException $e) {
            throw new ErrorException(
                errorCode: ErrorCode::InvalidResponseData,
                message: "Invalid Schema Response Data: {$e->getMessage()}"
            );
        }

        $variables = Arr::get($response, 'variables') ?? [];

        $variable_rules = [
            'variables' => ['nullable', 'array']
        ];

        foreach ($variables as $i => $item) {
            $variable_rules = [
                ...$variable_rules,
                ...VariablePresenter::getStructValidationRules($item, "variables.$i")
            ];
        }

        ArrayValidator::validate($response, $variable_rules, 'Variables');

        return new Schema($this->integration->code, $response, Carbon::now());
    }
}
