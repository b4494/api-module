<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Presenters;

use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Abstraction\Enums\MessengerType;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class MessengerInstancePresenter
{
    public static function getValidationRules(array $data, string $key): array
    {
        $key_data = Arr::get($data, $key);

        if ($key_data === null) {
            return [$key => ['required']];
        }

        if (is_array($key_data)) {
            return self::getStructValidationRules($key);
        }

        return [$key => ['required', 'string']];
    }

    public static function getStructValidationRules(string $key): array
    {
        return [
            "$key.externalId"   => ['required', 'string'],
            "$key.externalType" => ['required', 'string'],
            "$key.name"         => ['required', 'string'],
            "$key.description"  => ['nullable', 'string'],
            "$key.messengerId"  => ['nullable', 'string'],
            "$key.internalType" => ['nullable', Rule::in(array_column(MessengerType::cases(), 'name'))],
        ];
    }

    public static function make(array|string $data): MessengerInstance|string
    {
        if (is_array($data)) {
            return self::makeStruct($data);
        }

        return $data;
    }

    public static function makeStruct(array $data): MessengerInstance
    {
        return new MessengerInstance(
            externalType: Arr::get($data, 'externalType'),
            externalId: Arr::get($data, 'externalId'),
            name: Arr::get($data, 'name'),
            description: Arr::get($data, 'description'),
            messengerId: Arr::get($data, 'messengerId'),
            internalType: static::mapInternalType($data['internalType'] ?? null),
            extraData: $data['extraData'] ?? null,
        );
    }

    public static function mapInternalType(mixed $param): ?MessengerType
    {
        if (!$param) return null;

        foreach (MessengerType::cases() as $case) {
            if ($case->name === $param) return $case;
        }

        return null;
    }
}
