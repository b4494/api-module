<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Presenters;

use BmPlatform\Abstraction\DataTypes\Tag;
use Illuminate\Support\Arr;

class TagPresenter
{
    public static function getValidationRules(array $data, string $key): array
    {
        $key_data = Arr::get($data, $key);

        if ($key_data === null) {
            return [$key => ['required']];
        }

        if (is_array($key_data)) {
            return self::getStructValidationRules($key);
        }

        return [$key => ['required', 'string']];
    }

    public static function getStructValidationRules(string $key): array
    {
        return [
            "$key.externalId" => ['required', 'string'],
            "$key.label"      => ['required', 'string'],
            "$key.group"      => ['nullable', 'string'],
        ];
    }

    public static function make(array|string $data): Tag|string
    {
        if (is_array($data)) {
            return self::makeStruct($data);
        }

        return $data;
    }

    public static function makeStruct(array $data): Tag
    {
        return new Tag(
            externalId: Arr::get($data, 'externalId'),
            label: Arr::get($data, 'label'),
            group: Arr::get($data, 'group')
        );
    }
}
