<?php

namespace BmPlatform\ApiModule\Presenters;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\PaymentStatus;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class AppExternalStatusPresenter
{
    public static function rules(string $key = ''): array
    {
        return [
            $key => [ 'filled', 'array' ],
            $key.'status' => [ 'required_with:'.$key, Rule::in(Arr::pluck(AppStatus::cases(), 'value')) ],
            $key.'paymentStatus' => [ 'filled', Rule::in(Arr::pluck(PaymentStatus::cases(), 'value')) ],
            $key.'expiresAt' => [ 'filled', 'date_format:'.\DateTimeInterface::RFC3339_EXTENDED ],
        ];
    }

    public static function makeStruct(array $data): AppExternalStatus
    {
        return new AppExternalStatus(
            status: AppStatus::tryFrom($data['status']),
            paymentStatus: PaymentStatus::tryFrom($data['paymentStatus'] ?? ''),
            expiresAt: isset($data['expiresAt']) ? Carbon::parse($data['expiresAt']) : null,
        );
    }
}