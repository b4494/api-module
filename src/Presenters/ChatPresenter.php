<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Presenters;

use BmPlatform\Abstraction\DataTypes\Chat;
use Illuminate\Support\Arr;

class ChatPresenter
{
    public static function getValidationRules(array $data, string $key): array
    {
        $key_data = Arr::get($data, $key);

        if ($key_data === null) {
            return [$key => ['required']];
        }

        if (is_array($key_data)) {
            return self::getStructValidationRules($data, $key);
        }

        return [$key => ['required', 'string']];
    }

    public static function getStructValidationRules(array $data, string $key): array
    {
        $contact_rules = [];

        if (Arr::has($data, "$key.contact")) {
            $contact_rules = ContactPresenter::getValidationRules($data, "$key.contact");
        }

        $operator_rules = [];

        if (Arr::has($data, "$key.operator")) {
            $operator_rules = OperatorPresenter::getValidationRules($data, "$key.operator");
        }

        return [
            "$key.externalId" => ['required', 'string'],
            ...MessengerInstancePresenter::getValidationRules($data, "$key.messengerInstance"),
            ...$contact_rules,
            ...$operator_rules,
            "$key.messengerId" => ['nullable', 'string'],
            "$key.extraData"   => ['nullable', 'array']
        ];
    }

    public static function make(array|string $data): Chat|string
    {
        if (is_array($data)) {
            return self::makeStruct($data);
        }

        return $data;
    }

    public static function makeStruct(array $data): Chat
    {
        return new Chat(
            externalId: Arr::get($data, 'externalId'),
            messengerInstance: MessengerInstancePresenter::make(Arr::get($data, 'messengerInstance')),
            contact: Arr::has($data, 'contact') ? ContactPresenter::make(Arr::get($data, 'contact')) : null,
            operator: Arr::has($data, 'operator') ? OperatorPresenter::make(Arr::get($data, 'operator')) : null,
            messengerId: Arr::get($data, 'messengerId'),
            extraData: Arr::get($data, 'extraData')
        );
    }
}
