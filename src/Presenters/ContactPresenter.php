<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Presenters;

use BmPlatform\Abstraction\DataTypes\Contact;
use Illuminate\Support\Arr;

class ContactPresenter
{
    public static function getValidationRules(array $data, string $key): array
    {
        $key_data = Arr::get($data, $key);

        if ($key_data === null) {
            return [$key => ['required']];
        }

        if (is_array($key_data)) {
            return self::getStructValidationRules($key);
        }

        return [$key => ['required', 'string']];
    }

    public static function getStructValidationRules(string $key): array
    {
        return [
            "$key.externalId"  => ['required', 'string'],
            "$key.name"        => ['nullable', 'string'],
            "$key.email"       => ['nullable', 'email'],
            "$key.phone"       => ['nullable', 'string'],
            "$key.avatarUrl"   => ['nullable', 'url'],
            "$key.extraFields" => ['nullable', 'array'],
            "$key.messengerId" => ['nullable', 'string'],
            "$key.extraData"   => ['nullable', 'array'],
        ];
    }

    public static function make(array|string $data): Contact|string
    {
        if (is_array($data)) {
            return self::makeStruct($data);
        }

        return $data;
    }

    public static function makeStruct(array $data): Contact
    {
        return new Contact(
            externalId: Arr::get($data, 'externalId'),
            name: Arr::get($data, 'name'),
            phone: Arr::get($data, 'phone'),
            email: Arr::get($data, 'email'),
            avatarUrl: Arr::get($data, 'avatarUrl'),
            extraFields: Arr::get($data, 'extraFields'),
            messengerId: Arr::get($data, 'messengerId'),
            extraData: Arr::get($data, 'extraData')
        );
    }
}
