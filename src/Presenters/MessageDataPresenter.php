<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Presenters;

use BmPlatform\Abstraction\DataTypes\GeoLocation;
use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\Enums\MediaFileType;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class MessageDataPresenter
{
    public static function getStructValidationRules(string $key): array
    {
        return [
            "$key.externalId"         => ['required', 'string'],
            "$key.text"               => ['required', 'string'],
            "$key.attachments"        => ['nullable', 'array'],
            "$key.attachments.*.type" => ['required', Rule::in(array_column(MediaFileType::cases(), 'name'))],
            "$key.attachments.*.url"  => ['required', 'url'],
            "$key.attachments.*.mime" => ['nullable', 'string'],
            "$key.date"               => ['nullable', 'date'],
            "$key.extraData"          => ['nullable', 'array'],
            "$key.geoLocation"        => ['nullable', 'array:lat,long'],
            "$key.geoLocation.lat"    => ['string'],
            "$key.geoLocation.long"   => ['string']
        ];
    }

    public static function makeStruct(array $data): MessageData
    {
        $date = null;

        if (Arr::has($data, 'date')) {
            $date = Carbon::parse(Arr::get($data, 'date'));
        }

        $attachments = null;

        if (Arr::has($data, 'attachments')) {
            $attachments = array_map(fn (array $item) => new MediaFile(
                type: self::getAttachmentTypeByName(Arr::get($item, 'type')),
                url: Arr::get($item, 'url'),
                mime: Arr::get($item, 'mime')
            ), Arr::get($data, 'attachments', []));
        }

        $geo_location = null;

        if (Arr::has($data, 'geoLocation')) {
            $geo_location = new GeoLocation(
                lat: Arr::get($data, 'geoLocation.lat'),
                long: Arr::get($data, 'geoLocation.long'),
            );
        }

        return new MessageData(
            externalId: Arr::get($data, 'externalId'),
            text: Arr::get($data, 'text'),
            attachments: $attachments,
            geoLocation: $geo_location,
            date: $date,
            extraData: Arr::get($data, 'extraData')
        );
    }

    private static function getAttachmentTypeByName(string $name): MediaFileType
    {
        $cases_keyed_by_name = array_combine(array_column(MediaFileType::cases(), 'name'), MediaFileType::cases());

        return Arr::get($cases_keyed_by_name, $name);
    }
}
