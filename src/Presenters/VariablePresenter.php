<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Presenters;

use BmPlatform\Abstraction\Enums\VariableCategory;
use BmPlatform\ApiModule\Entities\Variables\ComplexVariable;
use BmPlatform\ApiModule\Entities\Variables\EnumVariable;
use BmPlatform\ApiModule\Entities\Variables\EnumVariableOption;
use BmPlatform\ApiModule\Entities\Variables\Variable;
use BmPlatform\ApiModule\Entities\Variables\VariableResolver;
use Closure;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class VariablePresenter
{
    public static function getStructValidationRules(array $data, string $key): array
    {
        return [
            "$key.key" => ['required', function (string $attribute, mixed $value, Closure $fail) {
                if ($value !== Str::camel((string) $value)) {
                    $fail('The :attribute must be camelCase');
                }
            }],
            "$key.type"               => ['required', Rule::in(Variable::TYPES)],
            "$key.name"               => ['required', 'string'],
            "$key.category"           => ['nullable', 'string'],
            "$key.array"              => ['nullable', 'bool'],
            "$key.hideFromConditions" => ['nullable', 'bool'],

            ...self::getResolverRules($data, $key),
            ...self::getTypeSpecificRules($data, $key)
        ];
    }

    private static function getResolverRules(array $data, string $key): array
    {
        if (is_array(Arr::get($data, 'resolver'))) {
            return [
                "$key.resolver"             => ['nullable', 'array'],
                "$key.resolver.description" => ['nullable', 'string'],
                "$key.resolver.lifetime"    => ['required', 'int'],
                "$key.resolver.key"         => ['nullable', 'string']
            ];
        }

        return ["$key.resolver" => ['nullable', 'string']];
    }

    private static function getTypeSpecificRules(array $data, string $key): array
    {
        $type = Arr::get($data, 'type');

        if ($type === Variable::TYPE_ENUM) {
            return [
                "$key.options"         => ['required', 'array'],
                "$key.options.*"       => ['required', 'array', 'min:1'],
                "$key.options.*.value" => ['required', 'string'],
                "$key.options.*.label" => ['required', 'string'],
            ];
        }

        if ($type === Variable::TYPE_COMPLEX) {
            $rules = [
                "$key.properties"   => ['required', 'array'],
                "$key.properties.*" => ['required', 'array', 'min:1'],
            ];

            foreach (Arr::get($data, 'properties', []) as $i => $property) {
                $rules = [
                    ...$rules,
                    ...self::getStructValidationRules((array) $property, "$key.properties.$i")
                ];
            }

            return $rules;
        }

        return [];
    }

    public static function makeStruct(array $data): Variable
    {
        $type = Arr::get($data, 'type');

        $variable = null;

        if ($type === Variable::TYPE_ENUM) {
            $variable = new EnumVariable(
                key: Arr::get($data, 'key'),
                name: Arr::get($data, 'name'),
                options: array_map(fn (array $item) => new EnumVariableOption(label: Arr::get($item, 'label'), value: Arr::get($item, 'value')), Arr::get($data, 'options'))
            );
        }

        if ($type === Variable::TYPE_COMPLEX) {
            $variable = new ComplexVariable(
                key: Arr::get($data, 'key'),
                name: Arr::get($data, 'name'),
                properties: array_map(fn (array $item) => self::makeStruct($item), Arr::get($data, 'properties'))
            );
        }

        if ($variable === null) {
            $variable = new Variable(
                key: Arr::get($data, 'key'),
                type: Arr::get($data, 'type'),
                name: Arr::get($data, 'name'),
            );
        }

        $resolver = Arr::get($data, 'resolver');

        if (is_array($resolver)) {
            $resolver_data = $resolver;

            $resolver              = new VariableResolver(Arr::get($resolver_data, 'lifetime'));
            $resolver->key         = Arr::get($resolver_data, 'key');
            $resolver->description = Arr::get($resolver_data, 'description');
        }

        $variable->resolver = $resolver;

        if (isset($data['category'])) {
            $variable->category = VariableCategory::tryFrom($data['category']) ?? $data['category'];
        }

        $variable->array    = Arr::get($data, 'array');
        $variable->hide_from_conditions = Arr::get($data, 'hideFromConditions');

        return $variable;
    }
}
