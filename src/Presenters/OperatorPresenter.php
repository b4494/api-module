<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Presenters;

use BmPlatform\Abstraction\DataTypes\Operator;
use Illuminate\Support\Arr;

class OperatorPresenter
{
    public static function getValidationRules(array $data, string $key): array
    {
        $key_data = Arr::get($data, $key);

        if ($key_data === null) {
            return [$key => ['required']];
        }

        if (is_array($key_data)) {
            return self::getStructValidationRules($key);
        }

        return [$key => ['required', 'string']];
    }

    public static function getStructValidationRules(string $key): array
    {
        return [
            "$key.externalId" => ['required', 'string'],
            "$key.name"       => ['required', 'string'],
            "$key.phone"      => ['nullable', 'string'],
            "$key.email"      => ['nullable', 'string'],
            "$key.avatarUrl"  => ['nullable', 'url'],
            "$key.extraData"  => ['nullable', 'array'],
        ];
    }

    public static function make(array|string $data): Operator|string
    {
        if (is_array($data)) {
            return self::makeStruct($data);
        }

        return $data;
    }

    public static function makeStruct(array $data): Operator
    {
        return new Operator(
            externalId: Arr::get($data, 'externalId'),
            name: Arr::get($data, 'name'),
            phone: Arr::get($data, 'phone'),
            email: Arr::get($data, 'email'),
            avatarUrl: Arr::get($data, 'avatarUrl'),
            extraData: Arr::get($data, 'extraData')
        );
    }
}
