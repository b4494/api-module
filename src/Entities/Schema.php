<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities;

use BmPlatform\ApiModule\Entities\Variables\Variable;
use BmPlatform\ApiModule\Presenters\VariablePresenter;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class Schema
{
    public function __construct(
        public string $code,
        public array $schema_data,
        public Carbon $updated_at
    ) {
    }

    public function getSchema(): array
    {
        return Arr::except($this->schema_data, 'variables');
    }

    /**
     * @return Variable[]
     */
    public function getVariables(): array
    {
        return array_map(fn (array $item) => VariablePresenter::makeStruct($item), Arr::get($this->schema_data, 'variables', []));
    }
}
