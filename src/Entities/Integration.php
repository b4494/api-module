<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities;

class Integration
{
    public function __construct(
        public readonly string $code,
        public readonly string $url,
        public readonly string $key
    ) {
    }
}
