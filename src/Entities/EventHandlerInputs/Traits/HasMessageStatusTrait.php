<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits;

use BmPlatform\Abstraction\Enums\MessageStatus;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

trait HasMessageStatusTrait
{
    protected array $data;

    protected function getMessageStatusRules(): array
    {
        return [
            'status' => ['required', Rule::in(array_column(MessageStatus::cases(), 'name'))],
        ];
    }

    public function getMessageStatus(): MessageStatus
    {
        $cases_keyed_by_name = array_combine(array_column(MessageStatus::cases(), 'name'), MessageStatus::cases());

        return Arr::get($cases_keyed_by_name, Arr::get($this->data, 'status'));
    }
}
