<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits;

use Illuminate\Support\Arr;

trait HasTicketIdTrait
{
    protected array $data;

    protected function getTicketIdRules(): array
    {
        return [
            'ticketId' => ['required', 'string'],
        ];
    }

    public function getTicketId(): string
    {
        return Arr::get($this->data, 'ticketId');
    }
}
