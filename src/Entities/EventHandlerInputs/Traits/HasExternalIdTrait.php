<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits;

use Illuminate\Support\Arr;

trait HasExternalIdTrait
{
    protected array $data;

    protected function getExternalIdRules(): array
    {
        return [
            'externalId' => ['required', 'string'],
        ];
    }

    public function getExternalId(): string
    {
        return Arr::get($this->data, 'externalId');
    }
}
