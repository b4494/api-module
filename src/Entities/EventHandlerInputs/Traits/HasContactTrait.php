<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\ApiModule\Presenters\ContactPresenter;
use Illuminate\Support\Arr;

trait HasContactTrait
{
    protected array $data;

    protected function getContactRules(string $key = 'contact'): array
    {
        return ContactPresenter::getValidationRules($this->data, $key);
    }

    public function getContact(string $key = 'contact'): Contact|string
    {
        return ContactPresenter::make(Arr::get($this->data, $key));
    }
}
