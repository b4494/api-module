<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\ApiModule\Presenters\ChatPresenter;
use Illuminate\Support\Arr;

trait HasChatTrait
{
    protected array $data;

    public function getChatRules(): array
    {
        return ChatPresenter::getValidationRules($this->data, 'chat');
    }

    public function getChat(): Chat|string
    {
        return ChatPresenter::make(Arr::get($this->data, 'chat'));
    }
}
