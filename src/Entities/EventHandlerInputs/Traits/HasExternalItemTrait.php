<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits;

use BmPlatform\Abstraction\DataTypes\ExternalPlatformItem;
use Illuminate\Support\Arr;

trait HasExternalItemTrait
{
    protected array $data;

    protected function getExternalItemRules(): array
    {
        return [
            'externalItem'              => ['nullable', 'array'],
            'externalItem.externalId'   => ['nullable', 'string'],
            'externalItem.url'          => ['nullable', 'url'],
            'externalItem.title'        => ['nullable', 'string'],
            'externalItem.externalData' => ['nullable', 'array'],
        ];
    }

    public function getExternalPlatformItem(): ?ExternalPlatformItem
    {
        $external_platform_item = null;

        if (Arr::has($this->data, 'externalItem')) {
            $external_platform_item = new ExternalPlatformItem(
                externalId: Arr::get($this->data, 'externalItem.externalId'),
                url: Arr::get($this->data, 'externalItem.url'),
                title: Arr::get($this->data, 'externalItem.title'),
                extraData: Arr::get($this->data, 'externalItem.externalData')
            );
        }

        return $external_platform_item;
    }
}
