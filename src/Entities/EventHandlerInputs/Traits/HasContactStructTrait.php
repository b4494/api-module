<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\ApiModule\Presenters\ContactPresenter;
use Illuminate\Support\Arr;

trait HasContactStructTrait
{
    protected array $data;

    protected function getContactRules(): array
    {
        return ContactPresenter::getStructValidationRules('contact');
    }

    public function getContact(): Contact
    {
        return ContactPresenter::makeStruct(Arr::get($this->data, 'contact'));
    }
}
