<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits;

use BmPlatform\Abstraction\Enums\ErrorCode;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

trait HasErrorTrait
{
    protected array $data;

    protected function getErrorRules(): array
    {
        return [
            'errorCode'        => ['nullable', Rule::in(array_column(ErrorCode::cases(), 'value'))],
            'errorDescription' => ['nullable', 'string']
        ];
    }

    public function getErrorCode(): ?ErrorCode
    {
        return ErrorCode::tryFrom(Arr::get($this->data, 'errorCode') ?? '');
    }

    public function getErrorDescription(): ?string
    {
        return Arr::get($this->data, 'errorDescription');
    }
}
