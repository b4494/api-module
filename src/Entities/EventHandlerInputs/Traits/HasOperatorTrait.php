<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits;

use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\ApiModule\Presenters\OperatorPresenter;
use Illuminate\Support\Arr;

trait HasOperatorTrait
{
    protected array $data;

    protected function getOperatorRules(string $key = 'operator'): array
    {
        return OperatorPresenter::getValidationRules($this->data, $key);
    }

    public function getOperator(string $key = 'operator'): Operator|string
    {
        return OperatorPresenter::make(Arr::get($this->data, $key));
    }
}
