<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits;

use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\ApiModule\Presenters\MessageDataPresenter;
use Illuminate\Support\Arr;

trait HasMessageTrait
{
    protected array $data;

    protected function getMessageRules(): array
    {
        return MessageDataPresenter::getStructValidationRules('message');
    }

    public function getMessageData(): MessageData
    {
        return MessageDataPresenter::makeStruct(Arr::get($this->data, 'message'));
    }
}
