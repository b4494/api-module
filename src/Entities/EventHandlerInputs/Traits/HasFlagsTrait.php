<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits;

use BmPlatform\Abstraction\Enums\InboxFlags;
use Illuminate\Support\Arr;

trait HasFlagsTrait
{
    protected array $data;

    protected function getFlagsRules(): array
    {
        return [
            'flags'                     => ['nullable', 'array'],
            'flags.newTicketOpened'     => ['nullable', 'bool'],
            'flags.newChatCreated'      => ['nullable', 'bool'],
            'flags.externalPostComment' => ['nullable', 'bool'],
        ];
    }

    public function getFlags(): ?int
    {
        $flags = null;

        if (Arr::has($this->data, 'flags')) {
            $flags = 0;

            $flags_states = [
                InboxFlags::NEW_TICKET_OPENED     => Arr::get($this->data, 'flags.newTicketOpened', false),
                InboxFlags::NEW_CHAT_CREATED      => Arr::get($this->data, 'flags.newChatCreated', false),
                InboxFlags::EXTERNAL_POST_COMMENT => Arr::get($this->data, 'flags.externalPostComment', false),
            ];

            foreach ($flags_states as $flag => $state) {
                if ($state) {
                    $flags |= $flag;
                }
            }
        }

        return $flags;
    }
}
