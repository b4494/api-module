<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits;

use Illuminate\Support\Arr;

trait HasExtraDataTrait
{
    protected array $data;

    protected function getExtraDataRules(): array
    {
        return [
            'extraData' => ['nullable', 'array'],
        ];
    }

    public function getExtraData(): ?array
    {
        return Arr::get($this->data, 'extraData');
    }
}
