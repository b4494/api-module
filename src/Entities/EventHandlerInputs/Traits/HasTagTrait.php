<?php

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits;

use BmPlatform\Abstraction\DataTypes\Tag;
use BmPlatform\ApiModule\Presenters\TagPresenter;
use Illuminate\Support\Arr;

trait HasTagTrait
{
    protected array $data;

    protected function getTagRules(): array
    {
        return [
            ...TagPresenter::getValidationRules($this->data, 'tag'),
            'forTicket' => ['required', 'bool']
        ];
    }

    public function getTag(): string|Tag
    {
        return TagPresenter::make(Arr::get($this->data, 'tag'));
    }

    public function getForTicket(): bool
    {
        return Arr::get($this->data, 'forTicket');
    }
}
