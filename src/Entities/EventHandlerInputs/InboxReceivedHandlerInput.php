<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasChatTrait;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasContactTrait;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasExternalItemTrait;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasFlagsTrait;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasMessageTrait;

class InboxReceivedHandlerInput extends AbstractHandlerInput
{
    use HasChatTrait;
    use HasContactTrait {
        getContact as private;
    }
    use HasMessageTrait;
    use HasExternalItemTrait;
    use HasFlagsTrait;

    protected function getValidateRules(): array
    {
        return [
            ...$this->getChatRules(),
            ...$this->getContactRules('participant'),
            ...$this->getMessageRules(),
            ...$this->getExternalItemRules(),
            ...$this->getFlagsRules()
        ];
    }

    public function getParticipant(): Contact|string
    {
        return $this->getContact('participant');
    }
}
