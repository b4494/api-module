<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs;

use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasContactStructTrait;

class ContactDataUpdatedHandlerInput extends AbstractHandlerInput
{
    use HasContactStructTrait;

    protected function getValidateRules(): array
    {
        return $this->getContactRules();
    }
}
