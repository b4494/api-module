<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs;

use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasChatTrait;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasMessageTrait;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasOperatorTrait;

class OutboxSentHandlerInput extends AbstractHandlerInput
{
    use HasChatTrait;
    use HasMessageTrait;
    use HasOperatorTrait;

    public function getValidateRules(): array
    {
        return [
            ...$this->getChatRules(),
            ...$this->getMessageRules(),
            ...$this->getOperatorRules()
        ];
    }
}
