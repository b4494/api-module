<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs;

use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasChatTrait;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasTicketIdTrait;

class ChatTicketClosedHandlerInput extends AbstractHandlerInput
{
    use HasChatTrait;
    use HasTicketIdTrait;

    protected function getValidateRules(): array
    {
        return [
            ...$this->getChatRules(),
            ...$this->getTicketIdRules()
        ];
    }
}
