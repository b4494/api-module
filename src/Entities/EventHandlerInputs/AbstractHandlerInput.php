<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs;

use BmPlatform\ApiModule\Components\ArrayValidator;
use Carbon\Carbon;
use Illuminate\Support\Arr;

abstract class AbstractHandlerInput
{
    public function __construct(
        protected array $data
    ) {
        $this->validate($data);
    }

    protected function validate(array $data): void
    {
        $rules = [
            'timestamp' => ['nullable', 'numeric'],
            ...$this->getValidateRules()
        ];

        ArrayValidator::validate($data, $rules, 'Request');
    }

    abstract protected function getValidateRules(): array;

    public function getTimestamp(): ?Carbon
    {
        $timestamp = Arr::get($this->data, 'timestamp');

        return $timestamp === null ? null : Carbon::createFromTimestamp($timestamp);
    }
}
