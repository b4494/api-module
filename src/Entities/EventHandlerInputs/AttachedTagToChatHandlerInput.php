<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs;

use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasChatTrait;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasTagTrait;

class AttachedTagToChatHandlerInput extends AbstractHandlerInput
{
    use HasChatTrait;
    use HasTagTrait;

    protected function getValidateRules(): array
    {
        return [
            ...$this->getChatRules(),
            ...$this->getTagRules()
        ];
    }
}
