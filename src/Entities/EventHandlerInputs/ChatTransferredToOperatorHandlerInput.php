<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs;

use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasChatTrait;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasOperatorTrait;

class ChatTransferredToOperatorHandlerInput extends AbstractHandlerInput
{
    use HasChatTrait;
    use HasOperatorTrait {
        getOperator as private;
    }

    protected function getValidateRules(): array
    {
        return [
            ...$this->getChatRules(),
            ...$this->getOperatorRules('prevOperator'),
            ...$this->getOperatorRules('newOperator')
        ];
    }

    public function getPrevOperator(): Operator|string
    {
        return $this->getOperator('prevOperator');
    }

    public function getNewOperator(): Operator|string
    {
        return $this->getOperator('newOperator');
    }
}
