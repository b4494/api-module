<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs;

use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasChatTrait;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasErrorTrait;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasExternalIdTrait;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasMessageStatusTrait;

class MessageStatusChangedHandlerInput extends AbstractHandlerInput
{
    use HasChatTrait;
    use HasExternalIdTrait;
    use HasMessageStatusTrait;
    use HasErrorTrait;

    protected function getValidateRules(): array
    {
        return [
            ...$this->getChatRules(),
            ...$this->getExternalIdRules(),
            ...$this->getMessageStatusRules(),
            ...$this->getErrorRules()
        ];
    }
}
