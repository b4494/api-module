<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs;

use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasChatStructTrait;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasContactStructTrait;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasExtraDataTrait;

class NewContactChatAddedHandlerInput extends AbstractHandlerInput
{
    use HasChatStructTrait;
    use HasContactStructTrait;
    use HasExtraDataTrait;

    protected function getValidateRules(): array
    {
        return [
            ...$this->getChatRules(),
            ...$this->getContactRules(),
            ...$this->getExtraDataRules()
        ];
    }
}
