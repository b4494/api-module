<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\EventHandlerInputs;

use BmPlatform\ApiModule\Entities\EventHandlerInputs\Traits\HasChatStructTrait;

class ChatDataChangedHandlerInput extends AbstractHandlerInput
{
    use HasChatStructTrait;

    protected function getValidateRules(): array
    {
        return $this->getChatRules();
    }
}
