<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\Variables;

class EnumVariable extends Variable
{
    /**
     * @param string $key
     * @param string $name
     * @param EnumVariableOption[] $options
     */
    public function __construct(string $key, string $name, public array $options)
    {
        parent::__construct($key, Variable::TYPE_ENUM, $name);
    }

    public function optionsToArray(): array
    {
        return array_map(fn (EnumVariableOption $option) => ['label' => $option->label, 'value' => $option->value], $this->options);
    }
}
