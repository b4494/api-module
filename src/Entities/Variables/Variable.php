<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\Variables;

use BmPlatform\Abstraction\Enums\VariableCategory;

class Variable
{
    public const TYPE_ENUM    = 'enum';
    public const TYPE_COMPLEX = 'complex';

    public const TYPES = [
        'text', 'mixed', 'number', 'int', 'float', 'bool', 'datetime', 'date', 'time', 'money', 'phone', 'pk', self::TYPE_ENUM, 'tag', self::TYPE_COMPLEX
    ];

    public string|VariableResolver|null $resolver = null;
    public string|VariableCategory|null $category = null;
    public ?bool $hide_from_conditions = null;
    public ?bool $array = null;

    public function __construct(
        public string $key,
        public string $type,
        public string $name,
    ) {
    }
}
