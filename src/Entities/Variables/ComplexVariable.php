<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\Variables;

class ComplexVariable extends Variable
{
    /**
     * @param string $key
     * @param string $name
     * @param array<ComplexVariable|EnumVariable|Variable> $properties
     */
    public function __construct(string $key, string $name, public array $properties)
    {
        parent::__construct($key, Variable::TYPE_COMPLEX, $name);
    }
}
