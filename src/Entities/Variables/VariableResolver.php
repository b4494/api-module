<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\Variables;

class VariableResolver
{
    public ?string $description = null;
    public ?string $key = null;

    public function __construct(
        public int $lifetime
    ) {
    }
}
