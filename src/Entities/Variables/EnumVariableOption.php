<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Entities\Variables;

class EnumVariableOption
{
    public function __construct(
        public string $label,
        public string $value
    ) {
    }
}
