<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\EventHandlers;

use BmPlatform\Abstraction\Events\ChatTransferredToOperator;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ChatTransferredToOperatorHandlerInput;

class ChatTransferredToOperatorHandler extends AbstractHandler
{
    public function __invoke(ChatTransferredToOperatorHandlerInput $input): ChatTransferredToOperator
    {
        return new ChatTransferredToOperator(
            chat: $input->getChat(),
            newOperator: $input->getNewOperator(),
            prevOperator: $input->getPrevOperator(),
            timestamp: $input->getTimestamp()
        );
    }
}
