<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\EventHandlers;

use BmPlatform\Abstraction\Events\ContactDataUpdated;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ContactDataUpdatedHandlerInput;

class ContactDataUpdatedHandler extends AbstractHandler
{
    public function __invoke(ContactDataUpdatedHandlerInput $input): ContactDataUpdated
    {
        return new ContactDataUpdated(
            contact: $input->getContact(),
            timestamp: $input->getTimestamp()
        );
    }
}
