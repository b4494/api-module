<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\EventHandlers;

use BmPlatform\Abstraction\Events\MessageStatusChanged;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\MessageStatusChangedHandlerInput;

class MessageStatusChangedHandler extends AbstractHandler
{
    public function __invoke(MessageStatusChangedHandlerInput $input): MessageStatusChanged
    {
        return new MessageStatusChanged(
            chat: $input->getChat(),
            externalId: $input->getExternalId(),
            status: $input->getMessageStatus(),
            timestamp: $input->getTimestamp(),
            errorCode: $input->getErrorCode(),
            errorDescription: $input->getErrorDescription()
        );
    }
}
