<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\EventHandlers;

use BmPlatform\Abstraction\Events\InboxReceived;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\InboxReceivedHandlerInput;

class InboxReceivedHandler extends AbstractHandler
{
    public function __invoke(InboxReceivedHandlerInput $input): InboxReceived
    {
        return new InboxReceived(
            chat: $input->getChat(),
            participant: $input->getParticipant(),
            message: $input->getMessageData(),
            externalItem: $input->getExternalPlatformItem(),
            flags: $input->getFlags(),
            timestamp: $input->getTimestamp()
        );
    }
}
