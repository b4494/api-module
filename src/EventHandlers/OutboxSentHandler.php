<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\EventHandlers;

use BmPlatform\Abstraction\Events\OutboxSent;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\OutboxSentHandlerInput;

class OutboxSentHandler extends AbstractHandler
{
    public function __invoke(OutboxSentHandlerInput $input): OutboxSent
    {
        return new OutboxSent(
            chat: $input->getChat(),
            message: $input->getMessageData(),
            operator: $input->getOperator(),
            timestamp: $input->getTimestamp()
        );
    }
}
