<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\EventHandlers;

use BmPlatform\Abstraction\Events\DetachedTagFromChat;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\DetachedTagFromChatHandlerInput;

class DetachedTagFromChatHandler extends AbstractHandler
{
    public function __invoke(DetachedTagFromChatHandlerInput $input): DetachedTagFromChat
    {
        return new DetachedTagFromChat(
            chat: $input->getChat(),
            tag: $input->getTag(),
            forTicket: $input->getForTicket(),
            timestamp: $input->getTimestamp()
        );
    }
}
