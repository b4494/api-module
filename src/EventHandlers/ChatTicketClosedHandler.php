<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\EventHandlers;

use BmPlatform\Abstraction\Events\ChatTicketClosed;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ChatTicketClosedHandlerInput;

class ChatTicketClosedHandler extends AbstractHandler
{
    public function __invoke(ChatTicketClosedHandlerInput $input): ChatTicketClosed
    {
        return new ChatTicketClosed(
            chat: $input->getChat(),
            ticketId: $input->getTicketId(),
            timestamp: $input->getTimestamp()
        );
    }
}
