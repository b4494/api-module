<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\EventHandlers;

use BmPlatform\Abstraction\Events\NewContactChatAdded;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\NewContactChatAddedHandlerInput;

class NewContactChatAddedHandler extends AbstractHandler
{
    public function __invoke(NewContactChatAddedHandlerInput $input): NewContactChatAdded
    {
        return new NewContactChatAdded(
            chat: $input->getChat(),
            contact: $input->getContact(),
            extraData: $input->getExtraData(),
            timestamp: $input->getTimestamp()
        );
    }
}
