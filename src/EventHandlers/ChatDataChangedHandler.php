<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\EventHandlers;

use BmPlatform\Abstraction\Events\ChatDataChanged;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\ChatDataChangedHandlerInput;

class ChatDataChangedHandler extends AbstractHandler
{
    public function __invoke(ChatDataChangedHandlerInput $input): ChatDataChanged
    {
        return new ChatDataChanged(
            chat: $input->getChat(),
            timestamp: $input->getTimestamp()
        );
    }
}
