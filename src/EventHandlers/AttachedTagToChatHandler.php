<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\EventHandlers;

use BmPlatform\Abstraction\Events\AttachedTagToChat;
use BmPlatform\ApiModule\Entities\EventHandlerInputs\AttachedTagToChatHandlerInput;

class AttachedTagToChatHandler extends AbstractHandler
{
    public function __invoke(AttachedTagToChatHandlerInput $input): AttachedTagToChat
    {
        return new AttachedTagToChat(
            chat: $input->getChat(),
            tag: $input->getTag(),
            forTicket: $input->getForTicket(),
            timestamp: $input->getTimestamp()
        );
    }
}
