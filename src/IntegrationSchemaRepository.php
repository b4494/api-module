<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule;

use BmPlatform\ApiModule\Entities\Schema;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class IntegrationSchemaRepository
{
    /** @var array<string, Schema> */
    private array $schemas;

    /**
     * @return Schema[]
     */
    public function getAll(): array
    {
        if (!isset($this->schemas)) {
            $this->schemas = [];

            if (file_exists($this->getFilePath())) {
                foreach (json_decode(file_get_contents($this->getFilePath()), true) ?? [] as $schema) {
                    $code = (string) Arr::get($schema, 'code');

                    $this->schemas[$code] = new Schema(
                        code: $code,
                        schema_data: Arr::get($schema, 'schema_data'),
                        updated_at: Carbon::parse(Arr::get($schema, 'updated_at'))
                    );
                }
            }
        }

        return $this->schemas;
    }

    public function getByCode(string $code): ?Schema
    {
        return Arr::get($this->getAll(), $code);
    }

    /**
     * @param Schema[] $schemas
     * @return void
     */
    public function updateSchemas(array $schemas): void
    {
        $raw_data = [];

        foreach ($schemas as $schema) {
            $raw_data[] = [
                'code'        => $schema->code,
                'schema_data' => $schema->schema_data,
                'updated_at'  => $schema->updated_at->toDateTimeString()
            ];
        }

        file_put_contents($this->getFilePath(), json_encode($raw_data));
    }

    private function getFilePath(): string
    {
        return storage_path('app/schemas/api_module_schemas.json');
    }
}
