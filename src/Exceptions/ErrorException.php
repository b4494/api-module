<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Exceptions;

use BmPlatform\Abstraction\Enums\ErrorCode;

class ErrorException extends \BmPlatform\Abstraction\Exceptions\ErrorException
{
    public function __construct(ErrorCode $errorCode, string $message = '')
    {
        $message = "Api Module $message";

        parent::__construct($errorCode, $message);
    }
}
