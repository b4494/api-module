<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Components;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Exceptions\ErrorException;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Validator as LaravelValidator;

class ArrayValidator
{
    public static function validate(array $data, array $rules, string $data_description): void
    {
        $validator = new LaravelValidator(App::make(Translator::class), $data, $rules);

        if ($validator->fails()) {
            throw new ErrorException(
                errorCode: ErrorCode::DataMissing,
                message: "Invalid $data_description Data: {$validator->errors()->toJson()}"
            );
        }
    }
}
