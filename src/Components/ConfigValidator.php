<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule\Components;

use BmPlatform\ApiModule\ServiceProvider;
use Illuminate\Config\Repository;

class ConfigValidator
{
    public static function validate(Repository $config): void
    {
        ArrayValidator::validate($config->get(ServiceProvider::CONFIG_KEY, []), [
            'integrations'        => ['array'],
            'integrations.*.code' => ['required', 'string', 'distinct'],
            'integrations.*.url'  => ['required', 'url'],
            'integrations.*.key'  => ['required', 'string']
        ], 'Incorrect Integration json');
    }
}
