<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule;

use BmPlatform\Abstraction\Interfaces\Variable as VariableInterface;
use BmPlatform\Abstraction\Interfaces\RuntimeContext;
use BmPlatform\Abstraction\Interfaces\VariableRegistrar;
use BmPlatform\ApiModule\Entities\Variables\ComplexVariable;
use BmPlatform\ApiModule\Entities\Variables\EnumVariable;
use BmPlatform\ApiModule\Entities\Variables\Variable;
use BmPlatform\ApiModule\Entities\Variables\VariableResolver;
use Closure;
use Illuminate\Support\Arr;
use Throwable;

class VariablesRegistrarInjector
{
    public function __construct(
        private readonly VariableRegistrar $registrar
    ) {
    }

    public function inject(Variable|EnumVariable|ComplexVariable $variable): void
    {
        $app_variable = $this->registrarVariable($variable);

        if ($app_variable !== null) {
            $this->setAppVariableProperties($app_variable, $variable);
        }
    }

    private function registrarVariable(Variable|EnumVariable|ComplexVariable $variable): ?VariableInterface
    {
        if ($variable instanceof EnumVariable) {
            return $this->registrar->enum($variable->key, $variable->optionsToArray(), $this->buildVariableResolver($variable));
        }

        if ($variable instanceof ComplexVariable) {
            return $this->registrar->complex($variable->key, function (VariableRegistrar $registrar) use ($variable) {
                $injector = new VariablesRegistrarInjector($registrar);

                foreach ($variable->properties as $property) {
                    $injector->inject($property);
                }
            }, $this->buildVariableResolver($variable));
        }

        try {
            return $this->registrar->{$variable->type}($variable->key, $this->buildVariableResolver($variable));
        } catch (Throwable) {
        }

        return null;
    }

    private function buildVariableResolver(Variable|EnumVariable|ComplexVariable $variable): ?Closure
    {
        if ($variable->resolver instanceof VariableResolver) {
            return $this->getApiVariableResolverClosure(
                $variable->key,
                $variable->resolver->key ?? $variable->key,
                $variable->resolver
            );
        }

        if (is_string($variable->resolver)) {
            return $this->getExtractValueFromStructsResolverClosure($variable->resolver);
        }

        return null;
    }

    private function getApiVariableResolverClosure(string $variable_key, string $api_variable_key, VariableResolver $variable_resolver): Closure
    {
        return function (RuntimeContext $context) use ($variable_key, $api_variable_key, $variable_resolver) {
            return $context->cacheValue($variable_key, function () use ($context, $api_variable_key, $variable_resolver) {
                /** @var AppHandler $handler */
                $handler = $context->appInstance()->getHandler();

                return $handler->getApiCommands()->getChatVariableValue($context->chat()->getExternalId(), $api_variable_key, $variable_resolver->lifetime);
            });
        };
    }

    private function getExtractValueFromStructsResolverClosure(string $variable_resolver): Closure
    {
        return function (RuntimeContext $context) use ($variable_resolver) {
            $result = $context;

            foreach (explode('.', $variable_resolver) as $sub_path) {
                if (is_object($result)) {
                    foreach ([$sub_path, 'get' . ucfirst($sub_path)] as $possible_methods) {
                        if (method_exists($result, $possible_methods)) {
                            try {
                                $result = $result->{$possible_methods}();
                            } catch (Throwable) {
                                $result = null;
                            }

                            continue 2;
                        }
                    }
                }

                if (is_array($result)) {
                    $result = Arr::get($result, $sub_path);

                    continue;
                }

                $result = null;
            }

            return $result;
        };
    }

    private function setAppVariableProperties(VariableInterface $app_variable, Variable|EnumVariable|ComplexVariable $variable): void
    {
        $app_variable->name($variable->name);

        if ($variable->array !== null) {
            $app_variable->array($variable->array);
        }

        if ($variable->category !== null) {
            $app_variable->category($variable->category);
        }

        if ($variable->hide_from_conditions !== null) {
            $app_variable->hideFromConditions($variable->hide_from_conditions);
        }
    }
}
