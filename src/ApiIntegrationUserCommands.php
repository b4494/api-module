<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\DataTypes\InlineButton;
use BmPlatform\Abstraction\DataTypes\InlineUrlButton;
use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Abstraction\DataTypes\OperatorGroup;
use BmPlatform\Abstraction\DataTypes\QuickReply;
use BmPlatform\Abstraction\DataTypes\Tag;
use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Interfaces\Operator as OperatorInterface;
use BmPlatform\Abstraction\Interfaces\OperatorGroup as OperatorGroupInterface;
use BmPlatform\Abstraction\Requests\ChatTagRequest;
use BmPlatform\Abstraction\Requests\SendMediaRequest;
use BmPlatform\Abstraction\Requests\SendSystemMessageRequest;
use BmPlatform\Abstraction\Requests\SendTextMessageRequest;
use BmPlatform\Abstraction\Requests\TransferChatToOperatorRequest;
use BmPlatform\Abstraction\Requests\UpdateContactDataRequest;
use BmPlatform\Abstraction\Responses\MessageSendResult;
use BmPlatform\Abstraction\Responses\NewOperatorResponse;
use BmPlatform\ApiModule\Components\ArrayValidator;
use BmPlatform\ApiModule\Entities\Integration;
use BmPlatform\ApiModule\Entities\Variables\ComplexVariable;
use BmPlatform\ApiModule\Entities\Variables\EnumVariable;
use BmPlatform\ApiModule\Entities\Variables\Variable;
use BmPlatform\ApiModule\Presenters\AppExternalStatusPresenter;
use BmPlatform\ApiModule\Presenters\MessengerInstancePresenter;
use BmPlatform\ApiModule\Presenters\OperatorPresenter;
use BmPlatform\ApiModule\Presenters\TagPresenter;
use BmPlatform\ApiModule\Presenters\VariablePresenter;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;

class ApiIntegrationUserCommands
{
    private readonly ApiClient $client;

    public function __construct(Integration $integration, string $app_external_id)
    {
        $this->client = App::makeWith(ApiClient::class, [
            'base_url'   => "$integration->url/$app_external_id",
            'secret_key' => $integration->key
        ]);
    }

    public function getStatus(): AppExternalStatus
    {
        $response = $this->client->get('/status');

        $this->validateResponse(AppExternalStatusPresenter::rules(), $response);

        return AppExternalStatusPresenter::makeStruct($response);
    }

    /**
     * @return array<ComplexVariable|EnumVariable|Variable>
     */
    public function getExtraVariables(): array
    {
        $response = $this->client->get('/extraVariables');

        $rules = [
            '*' => ['nullable', 'array']
        ];

        foreach ($response ?? [] as $i => $item) {
            $rules = [
                ...$rules,
                ...VariablePresenter::getStructValidationRules($item, (string) $i)
            ];
        }

        $this->validateResponse($rules, $response);

        return array_map(fn (array $data) => VariablePresenter::makeStruct($data), $response);
    }

    /**
     * @param string|null $cursor
     * @return IterableData<MessengerInstance>
     */
    public function getMessengers(?string $cursor = null): IterableData
    {
        $response = $this->client->get('/messengers', [
            'cursor' => $cursor
        ]);

        $this->validateResponse([
            'items' => [ 'present', 'nullable', 'array'],
            ...MessengerInstancePresenter::getStructValidationRules('items.*'),
            'cursor' => ['nullable', 'filled', 'string'],
        ], $response);

        return new IterableData(
            array_map(
                fn (array $item) => MessengerInstancePresenter::makeStruct($item),
                Arr::get($response, 'items')
            ),
            Arr::get($response, 'cursor')
        );
    }

    /**
     * @param string|null $cursor
     * @return IterableData<Tag>
     */
    public function getTags(?string $cursor = null): IterableData
    {
        $response = $this->client->get('/tags', [
            'cursor' => $cursor
        ]);

        $this->validateResponse([
            'items' => ['required', 'array'],
            ...TagPresenter::getStructValidationRules('items.*'),
            'cursor' => ['nullable', 'filled', 'string']
        ], $response);

        return new IterableData(
            array_map(
                fn (array $item) => TagPresenter::makeStruct($item),
                Arr::get($response, 'items')
            ),
            Arr::get($response, 'cursor')
        );
    }

    /**
     * @param string|null $cursor
     * @return IterableData<Operator>
     */
    public function getOperators(?string $cursor = null): IterableData
    {
        $response = $this->client->get('/operators', [
            'cursor' => $cursor
        ]);

        $this->validateResponse([
            'items' => ['required', 'array'],
            ...OperatorPresenter::getStructValidationRules('items.*'),
            'cursor' => ['filled', 'string']
        ], $response);

        return new IterableData(
            array_map(
                fn (array $item) => OperatorPresenter::makeStruct($item),
                Arr::get($response, 'items')
            ),
            Arr::get($response, 'cursor')
        );
    }

    /**
     * @param string|null $cursor
     * @return IterableData<OperatorGroup>
     */
    public function getOperatorGroups(?string $cursor = null): IterableData
    {
        $response = $this->client->get('/operatorGroups', [
            'cursor' => $cursor
        ]);

        $this->validateResponse([
            'items'              => ['required', 'array'],
            'items.*.externalId' => ['required', 'string'],
            'items.*.name'       => ['required', 'string'],
            'items.*.extraData'  => ['nullable', 'array'],
            'cursor'             => ['nullable', 'filled', 'string']
        ], $response);

        return new IterableData(
            array_map(fn (array $item) => new OperatorGroup(
                externalId: Arr::get($item, 'externalId'),
                name: Arr::get($item, 'name'),
                extraData: Arr::get($item, 'extraData')
            ), Arr::get($response, 'items')),
            Arr::get($response, 'cursor')
        );
    }

    public function getChatVariableValue(string $chat_external_id, string $variable_key, int $ttl = 0): string
    {
        $response = $this->client->getCached("/chats/$chat_external_id/variables/$variable_key", [], $ttl);

        $this->validateResponse([
            'value' => ['required', 'string'],
        ], $response);

        return Arr::get($response, 'value');
    }

    public function sendTextMessage(SendTextMessageRequest $request): MessageSendResult
    {
        return $this->sendMessage('/sendTextMessage', [
            'chat'          => $this->serializeChat($request->chat),
            'quickReplies'  => $this->serializeQuickReplies($request->quickReplies),
            'inlineButtons' => $this->serializeInlineButtons($request->inlineButtons),
            'text'          => $request->text
        ]);
    }

    public function sendMediaMessage(SendMediaRequest $request): MessageSendResult
    {
        return $this->sendMessage('/sendMediaMessage', [
            'chat'          => $this->serializeChat($request->chat),
            'quickReplies'  => $this->serializeQuickReplies($request->quickReplies),
            'inlineButtons' => $this->serializeInlineButtons($request->inlineButtons),
            'file'          => [
                'type' => $request->file->type->name,
                'url'  => $request->file->url,
                'mime' => $request->file->mime
            ],
            'caption' => $request->caption
        ]);
    }

    public function sendSystemMessage(SendSystemMessageRequest $request): MessageSendResult
    {
        return $this->sendMessage('/sendSystemMessage', [
            'chat' => $this->serializeChat($request->chat),
            'text' => $request->text
        ]);
    }

    public function attachTagToChat(ChatTagRequest $request): void
    {
        $this->client->post('/attachTagToChat', [
            'chat'          => $this->serializeChat($request->chat),
            'tagExternalId' => is_string($request->tag) ? $request->tag : $request->tag->getExternalId(),
            'forTicket'     => $request->forTicket
        ]);
    }

    public function detachTagFromChat(ChatTagRequest $request): void
    {
        $this->client->post('/detachTagFromChat', [
            'chat'          => $this->serializeChat($request->chat),
            'tagExternalId' => is_string($request->tag) ? $request->tag : $request->tag->getExternalId(),
            'forTicket'     => $request->forTicket
        ]);
    }

    public function closeChatTicket(Chat $chat): void
    {
        $this->client->post('/closeChatTicket', [
            'chat' => $this->serializeChat($chat),
        ]);
    }

    public function transferChatToOperator(TransferChatToOperatorRequest $request): NewOperatorResponse
    {
        $target_type = null;
        $target_external_id = null;

        if ($request->target instanceof OperatorInterface) {
            $target_type        = 'operator';
            $target_external_id = $request->target->getExternalId();
        }

        if ($request->target instanceof OperatorGroupInterface) {
            $target_type        = 'operatorGroup';
            $target_external_id = $request->target->getName(); /** @todo replace by id */
        }

        $response = $this->client->post('/transferChatToOperator', [
            'targetType'       => $target_type,
            'targetExternalId' => $target_external_id,
            'messengerId'      => $request->chat->getMessengerId()
        ]);

        $this->validateResponse([
            'operator'    => ['required', 'string'],
            'messengerId' => ['nullable', 'string']
        ], $response);

        return new NewOperatorResponse(
            operator: Arr::get($response, 'operator')
        );
    }

    public function updateContactData(UpdateContactDataRequest $request): void
    {
        $this->client->post('/updateContactData', [
            'externalId'  => $request->contact->getExternalId(),
            'name'        => $request->name,
            'phone'       => $request->phone,
            'email'       => $request->email,
            'avatarUrl'   => $request->contact->getAvatarUrl(), /** @todo replace by field from request ? */
            'extraFields' => $request->fields,
            'messengerId' => $request->contact->getMessengerId(), /** @todo replace by field from request ? */
            'extraData'   => $request->contact->getExtraData() /** @todo replace by field from request ? */
        ]);
    }

    private function sendMessage(string $uri, array $data): MessageSendResult
    {
        $response = $this->client->post($uri, $data);

        $this->validateResponse([
            'externalId'  => ['required', 'string'],
            'messengerId' => ['required', 'string'],
        ], $response);

        return new MessageSendResult(
            externalId: Arr::get($response, 'externalId'),
            messengerId: Arr::get($response, 'messengerId')
        );
    }

    private function serializeChat(Chat $chat): array
    {
        return [
            'externalId'        => $chat->getExternalId(),
            'messengerInstance' => $chat->getMessengerInstance()->getExternalId(),
            'contact'           => $chat->getContact()?->getExternalId(),
            'operator'          => $chat->getOperator()?->getExternalId(),
            'messengerId'       => $chat->getMessengerId(),
            'extraData'         => $chat->getExtraData()
        ];
    }

    /**
     * @param QuickReply[][] $quick_replies_request
     * @return array
     */
    private function serializeQuickReplies(array $quick_replies_request): array
    {
        $quick_replies = [];

        foreach ($quick_replies_request as $i => $quick_replies_sub_array) {
            foreach ($quick_replies_sub_array as $j => $quick_reply) {
                $quick_replies[$i][$j] = [
                    'text'  => $quick_reply->text,
                    'type'  => $quick_reply->type->name,
                    'color' => $quick_reply->color?->name
                ];
            }
        }

        return $quick_replies;
    }

    /**
     * @param InlineButton[][]|InlineUrlButton[][] $inline_buttons_request
     * @return array
     */
    private function serializeInlineButtons(array $inline_buttons_request): array
    {
        $inline_buttons = [];

        foreach ($inline_buttons_request as $i => $inline_buttons_sub_array) {
            foreach ($inline_buttons_sub_array as $j => $inline_button) {
                $inline_buttons[$i][$j] = [
                    'text'    => $inline_button->text,
                    'payload' => $inline_button->payload,
                    'color'   => $inline_button->color?->name,
                    'url'     => is_a($inline_button, InlineUrlButton::class) ? $inline_button->url : null
                ];
            }
        }

        return $inline_buttons;
    }

    private function validateResponse(array $rules, mixed $response): void
    {
        ArrayValidator::validate($response, $rules, 'Response');
    }
}
