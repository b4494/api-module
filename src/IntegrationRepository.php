<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\ApiModule\Entities\Integration;
use BmPlatform\ApiModule\Exceptions\ErrorException;
use Illuminate\Config\Repository;
use Illuminate\Support\Arr;

class IntegrationRepository
{
    /** @var array<string, Integration> */
    private array $integrations;

    public function __construct(
        private readonly Repository $config
    ) {
    }

    /**
     * @return Integration[]
     */
    public function getAll(): array
    {
        if (!isset($this->integrations)) {
            $this->integrations = [];

            foreach ($this->config->get(ServiceProvider::CONFIG_KEY . '.integrations', []) as $integration) {
                $code = (string) Arr::get($integration, 'code');

                $this->integrations[$code] = new Integration(
                    code: $code,
                    url: Arr::get($integration, 'url'),
                    key: Arr::get($integration, 'key')
                );
            }
        }

        return $this->integrations;
    }

    public function getByCodeOrFail(string $code): Integration
    {
        $integration = Arr::get($this->getAll(), $code);

        if ($integration === null) {
            throw new ErrorException(ErrorCode::NotFound, 'Integration code is incorrect');
        }

        return $integration;
    }
}
