<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule;

use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Abstraction\Interfaces\VariableRegistrar;
use BmPlatform\ApiModule\Components\ConfigValidator;
use BmPlatform\ApiModule\Console\Commands\FetchIntegrationSchemasCommand;
use BmPlatform\ApiModule\Entities\Variables\Variable;
use Closure;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\App;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public const CONFIG_KEY = 'api_module_params';

    /**
     * @return void
     * @throws BindingResolutionException
     */
    public function boot(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/api_module_params.php', self::CONFIG_KEY);

//        ConfigValidator::validate($this->app['config']);

        $this->loadRoutesFrom(__DIR__ . '/Http/routes.php');

        if ($this->app->runningInConsole()) {
            $this->commands([
                FetchIntegrationSchemasCommand::class
            ]);
        }
        $this->registerAppTypes();
    }

    /**
     * @return mixed
     * @throws BindingResolutionException
     */
    protected function makeHub(): Hub
    {
        return $this->app->make(Hub::class);
    }

    private function getSchemaClosure(string $code): Closure
    {
        return static function () use ($code) {
            return static::getSchemaRepo()->getByCode($code)?->getSchema() ?: [];
        };
    }

    /**
     * @param Variable[] $variables
     * @return Closure
     */
    private function getRegisterVariablesClosure(string $code): Closure
    {
        return static function (VariableRegistrar $registrar) use ($code) {
            $injector = new VariablesRegistrarInjector($registrar);

            foreach (static::getSchemaRepo()->getByCode($code)?->getVariables() as $variable) {
                $injector->inject($variable);
            }
        };
    }

    private function getRegisterInstanceVariablesClosure(): Closure
    {
        return function (VariableRegistrar $registrar, AppInstance $app_instance) {
            /** @var AppHandler $app_handler */
            $app_handler = $app_instance->getHandler();

            $injector = new VariablesRegistrarInjector($registrar);

            foreach ($app_handler->getApiCommands()->getExtraVariables() as $variable) {
                $injector->inject($variable);
            }
        };
    }

    /**
     * @return mixed
     */
    protected static function getSchemaRepo(): IntegrationSchemaRepository
    {
        return App::make(IntegrationSchemaRepository::class);
    }

    /**
     * @param \BmPlatform\ApiModule\Entities\Integration $integration
     *
     * @return array
     */
    protected function integrationOptions(Entities\Integration $integration): array
    {
        return [
            'schema' => $this->getSchemaClosure($integration->code),
            'registerVariables' => $this->getRegisterVariablesClosure($integration->code),
            'registerInstanceVariables' => $this->getRegisterInstanceVariablesClosure(),
        ];
    }

    protected function registerAppTypes(): void
    {
        /** @var IntegrationRepository $integration_repository */
        $integration_repository = App::make(IntegrationRepository::class);

        $hub = $this->makeHub();

        foreach ($integration_repository->getAll() as $integration) {
            $hub->registerAppType(
                $integration->code,
                static fn (AppInstance $app) => new AppHandler($app, $integration),
                $this->integrationOptions($integration),
            );
        }
    }
}
