<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Abstraction\Interfaces\AppHandler as AppHandlerInterface;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Commands\Commands as CommandsInterface;
use BmPlatform\Abstraction\Interfaces\Features\HasOperatorGroups;
use BmPlatform\Abstraction\Interfaces\Features\HasOperators;
use BmPlatform\Abstraction\Interfaces\Features\HasTags;
use BmPlatform\ApiModule\Entities\Integration;
use Illuminate\Support\Facades\App;

class AppHandler implements AppHandlerInterface, HasOperators, HasOperatorGroups, HasTags
{
    private ApiIntegrationUserCommands $api_commands;
    private Commands $commands;

    public function __construct(
        private readonly AppInstance $app_instance,
        private readonly Integration $integration
    ) {
    }

    public function getMessengerInstances(mixed $cursor = null): IterableData
    {
        return $this->getApiCommands()->getMessengers($cursor);
    }

    public function getOperators(mixed $cursor = null): IterableData
    {
        return $this->wrap(fn () => $this->getApiCommands()->getOperators($cursor));
    }

    protected function wrap(callable $callback): IterableData
    {
        try {
            return $callback();
        }

        catch (ErrorException $e) {
            if ($e->errorCode == ErrorCode::FeatureNotSupported) return new IterableData([], null);

            throw $e;
        }
    }

    public function getOperatorGroups(mixed $cursor = null): IterableData
    {
        return $this->wrap(fn () => $this->getApiCommands()->getOperatorGroups($cursor));
    }

    public function getTags(mixed $cursor = null): IterableData
    {
        return $this->wrap(fn () => $this->getApiCommands()->getTags($cursor));
    }

    public function getCommands(): CommandsInterface
    {
        if (!isset($this->commands)) {
            $this->commands = App::make(Commands::class, [
                'api_commands' => $this->getApiCommands()
            ]);
        }

        return $this->commands;
    }

    /** @todo implement */
    public function activate(): void
    {
    }

    /** @todo implement */
    public function deactivate(): void
    {
    }

    public function status(): AppExternalStatus
    {
        // TODO: Implement status() method.
    }

    public function getApiCommands(): ApiIntegrationUserCommands
    {
        if (!isset($this->api_commands)) {
            $this->api_commands = App::make(ApiIntegrationUserCommands::class, [
                'integration'     => $this->integration,
                'app_external_id' => $this->app_instance->getExternalId(),
            ]);
        }

        return $this->api_commands;
    }
}
