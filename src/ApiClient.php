<?php

declare(strict_types=1);

namespace BmPlatform\ApiModule;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Support\Http\HttpClient;
use BmPlatform\ApiModule\Exceptions\ErrorException;
use Illuminate\Support\Arr;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;

class ApiClient extends HttpClient
{
    public const CACHE_PREFIX = 'api_module_';

    private array $cached = [];

    public function __construct(
        private readonly string $base_url,
        private readonly string $secret_key,
        private readonly CacheInterface $cache,
        private readonly LoggerInterface $logger,
        array $options = []
    ) {
        parent::__construct($options);
    }

    public function get(string $uri, array $query = []): mixed
    {
        return parent::get($this->base_url . $uri, [
            'query'   => $query,
            'headers' => [
                'Authorization' => "Bearer $this->secret_key",
                'Accept' => 'application/json',
            ]
        ]);
    }

    public function post(string $uri, array $body): mixed
    {
        return parent::post($this->base_url . $uri, [
            'json'    => $body,
            'headers' => [
                'Authorization' => "Bearer $this->secret_key"
            ]
        ]);
    }

    public function getCached(string $uri, array $query = [], int $ttl = 86400): mixed
    {
        $key = self::CACHE_PREFIX . 'GET' . $uri . '_' . http_build_query($query);

        if (!isset($this->cached[$key])) {
            $external_cache_result = $this->cache->get($key);

            if ($external_cache_result === null) {
                $external_cache_result = $this->get($uri, $query);

                if ($ttl > 0) {
                    $this->cache->set($key, $external_cache_result, $ttl);
                }
            }

            $this->cached[$key] = $external_cache_result;
        }

        return $this->cached[$key];
    }

    public function request(string $method, string $path, array $options = []): mixed
    {
        $this->logger->info('Api Module Request', [
            'request' => func_get_args(),
        ]);

        return parent::request($method, $path, $options);
    }

    protected function processResponse(ResponseInterface $response): mixed
    {
        $content = $response->getBody()->getContents();

        $this->logger->info('Api Module Response', [
            'code'     => $response->getStatusCode(),
            'response' => $content
        ]);

        $content_decoded = json_decode($content, true);

        if (in_array($response->getStatusCode(), [200, 204])) {
            return $content_decoded;
        }

        $error_code = ErrorCode::tryFrom(Arr::get($content_decoded, 'code') ?? '') ?? match ($response->getStatusCode()) {
            404 => ErrorCode::FeatureNotSupported,
            500 => ErrorCode::InternalServerError,
            502 => ErrorCode::ServiceUnavailable,
            default => ErrorCode::UnexpectedServerError,
        };

        throw new ErrorException($error_code, Arr::get($content_decoded, 'message', 'Unexpected Error'));
    }
}
