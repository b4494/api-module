<?php

declare(strict_types=1);

return [
    'integrations' => json_decode((string) env('API_MODULE_INTEGRATIONS', '[]'), true)
];
